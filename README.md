#####Warning

This is a personal project and the core concept still in development.

#OSRM
This project allow us to map keywords to functions and use them as a fluent API. Its main purpose is to create SQL queries purely in javascript and map the result to dynamic objects. The idea is to use the query to generate the object and not the object to generate the query (like ORMs do), so when creating the query it is possible to specify how the resulting object would be mapped.

Example:
```javascript
const Ac = SQL.Account();
const Cr = SQL.Credentials();
const Pr = SQL.Profile();
const Ph = SQL.Photo();
const Ct = SQL.Category();
const CP = SQL.CategoryProfile();

const {SELECT, FROM, INNER, LEFT, WHERE} = qry;

    SELECT  ( Ac, Pr, Ph, Ct ) .FROM ( Ac )
    INNER .JOIN ( Pr, Ac.profile )          // The first parameter is the usual "table" being joined and the second one is the target property used to load the resulting object into
      ON .EQUALS ( Ac.profileId, Pr.id )
    INNER .JOIN ( Ph, Pr.photo )
      ON .EQUALS ( Pr.photoId, Ph.id );
    LEFT .JOIN  ( CP )
      ON .EQUALS ( CP.profileId, Pr.id );
    LEFT .JOIN  ( Ct, Pr.categories.many )  // In this case the second parameter is used with a ".many", so the resulting object will be an array of Category mapped to the "categories" property
      ON .EQUALS( CP.categoryId, Ct.id )
    WHERE .EQUALS ( Ac.id, accountId );
```

The main advantage about using pure javascript to write the queries is, obviously, the fact that creating new features becomes pretty easy:
```javascript
function DATE_BETWEEN (qry, field, minDate, maxDate) {
  if(!isNaN(maxDate) && !isNaN(minDate)) {
    qry. COL(field) .BETWEEN (minDate, maxDate);

  } else if(!isNaN(minDate)) {
    qry .GT (field, minDate);

  } else if(!isNaN(maxDate)) {
    qry .LT (field, maxDate);
  }

  return qry;
}
```
registering keyword:

```javascript
[...]
  VALS: {
    head: '(',
    tail: ')',
    min: 1
  },
  ASSIGN: {
    bodySeparator: '=',
    min: 2,
    max: 2,
    prefix: false,
    handler: 'assign'
  },
[...]
```

##### Notes
I intend to rewrite the library core to work in a more immutable and functional way.

Improvements:
* The "keywords" call its callbacks immediately, it would be better to first create the whole "phrase" and only then call the callbacks. Doing so give me some optimization options and solve problems with dependent keywords.
* Once it was intended to be used to generate SQL queries in javascript, I developed the SQL module alongside with the core and, even though it is a plugin inside the library, some decisions were taken with the SQL module in mind.
