'use strict';

const SQLPlugin = require('./sql/plugin-index');
const Log = require('./lib/simple-logger');
const util = require('./lib/util');
let plugins = [];
let OSRM = require('./lib/osrm');
const toExport = {
  SQLPlugin: SQLPlugin,
  register: register,
  create: create
};

function register (plugin) {
  plugins.push(plugin);
};

function create(config) {
  config = Object.assign({}, { exportsAsRoot: true }, config);
  Log.setLoggerLevel(config.log);
  if(plugins.length === 0) {
    plugins.push(new SQLPlugin());
  }

  OSRM.register(plugins);

  if(config.exportsAsRoot) {
    module.exports = OSRM;
  } else {
    toExport.OSRM = OSRM;
  }

  plugins = [];
  return OSRM;
}

module.exports = toExport;
