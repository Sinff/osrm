'use strict';

const expect = require('chai').expect;
const KEY = 'test-osrm';
const Field = require('../sql/schema/schema-field');
const Schema = require('../sql/schema/schema');

describe('Unit: sql/schema-field & sql/schema', function() {

  it('Should add fields to the schema', function() {
    let baseSchema = {
      SOURCE: 'BASE',
      name: 'name',
      createdAt: 'created_at'
    };

    baseSchema = new Schema(baseSchema);
    expect(baseSchema.size).to.be.equals(2);

    let user = new Schema('USER', {});
    expect(user.size).to.be.equals(0);

    // add from other schema
    user.addField (baseSchema.fields.name);
    expect(user.size).to.be.equals(1);
    expect(user.fields.name).to.not.be.equals(baseSchema.fields.name);
    expect(user.fields.name.hasSameProps(baseSchema.fields.name)).to.be.true;

    // add a raw field
    const col1 = {field: 'email', property: 'email'};
    user.addField( col1 );
    expect(user.size).to.be.equals(2);
    expect(user.fields.email).to.be.ok;
    expect(user.fields.email.isKey).to.be.false;

    // replace the existing column
    let oldEmail = user.fields.email;
    user.addField( col1 );
    expect(user.size).to.be.equals(2);
    expect(oldEmail).to.not.be.equals(user.fields.email);
    expect(oldEmail.hasSameProps(user.fields.email)).to.be.true;

    // add a clone with modified key prop
    oldEmail = user.fields.email;
    user.addField( user.fields.email.key );
    expect(user.size).to.be.equals(2);
    expect(oldEmail).to.not.be.equals(user.fields.email);
    expect(oldEmail.hasSameProps(user.fields.email)).to.be.false;

  });

  it('Should remove fields from the schema', function() {
    let baseSchema = {
      SOURCE: 'BASE',
      name: 'name',
      createdAt: 'created_at'
    };

    baseSchema = new Schema(baseSchema);
    expect(baseSchema.size).to.be.equals(2);
    expect(baseSchema.fields.name).to.be.ok;
    expect(baseSchema.fields.createdAt).to.be.ok;

    // remove field
    baseSchema.removeField(baseSchema.fields.name);
    expect(baseSchema.size).to.be.equals(1);
    expect(baseSchema.fields.name).to.not.be.ok;

    // should do nothing
    baseSchema.removeField({property: 'notExistentField'});
    expect(baseSchema.size).to.be.equals(1);

    // remove with raw field
    baseSchema.removeField({property: 'createdAt'});
    expect(baseSchema.size).to.be.equals(0);
    expect(baseSchema.fields.createdAt).to.not.be.ok;
  });

  it('Should extract the key from row', function() {
    let baseSchema = {
      SOURCE: 'BASE',
      id: {field: '_id', isKey: true},
      name: 'name',
      createdAt: 'created_at'
    };
    let _schema = new Schema(baseSchema);
    const row = { BASE__id: 30, BASE_name: 'teste' };
    let key = _schema.extractKey(row);
    expect(key).to.be.equals('30');

    // turns the name into a key too
    _schema.addField(_schema.fields.name.key)
    key = _schema.extractKey(row);
    expect(key).to.be.equals('30teste');

    // change the base schema to have no keys anymore
    baseSchema.id = '_id';
    _schema = new Schema(baseSchema);
    expect(_schema.fields.id.isKey).to.be.false;

    // throw error due no keys found
    const throwError = () => {_schema.extractKey(row);}
    expect(throwError).to.throw(Error);
  });

  it('Should extract data from row', function() {
    let nestedSchema = {
      SOURCE: 'NESTED',
      id: 'nest_id',
      name: {field: 'test', isKey: true},
      mnest: {}
    };

    let baseSchema = {
      SOURCE: 'BASE',
      id: {field: '_id', isKey: true},
      name: 'name',
      createdAt: 'created_at'
    };

    const manyNested = {
      SOURCE: 'MANY',
      id: {field: 'an_id', isKey: true},
      prop: 'prop'
    };

    const rows = [
      { BASE__id: 12, BASE_name: 'teste', BASE_created_at: 123123123 },
      { BASE__id: 12, BASE_name: 'teste', BASE_created_at: 123123123, NESTED_nest_id: 37, NESTED_test: 'Nested name' },
      { BASE__id: 12, BASE_name: 'teste', BASE_created_at: 123123123, NESTED_nest_id: 37, NESTED_test: 'Nested name', MANY_an_id: 200, MANY_prop: 'prop' },
    ];

    const expBase = { id: 12, name: 'teste', createdAt: 123123123};
    const expNest = { id: 37, name: 'Nested name' };
    const expMany = { id: 200, prop: 'prop' };

    const expBaseNest = { id: 12, name: 'teste', createdAt: 123123123, nested: { id: 37, name: 'Nested name' } };
    const expManyNest = { id: 37, name: 'Nested name', mnest: [{ id: 200, prop: 'prop' }] };
    const expBaseNestMany = { id: 12, name: 'teste', createdAt: 123123123, nested: { id: 37, name: 'Nested name', mnest: [{ id: 200, prop: 'prop' }] } }

    let _schema = new Schema(baseSchema);
    let _nested = new Schema(nestedSchema);
    let _many   = new Schema(manyNested);
    let output = {};

    _schema.resolve(rows[0], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expBase);
    output = {}
    _schema.resolve(rows[1], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expBase);
    output = {}
    _schema.resolve(rows[2], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expBase);
    output = {}

    _nested.resolve(rows[0], {}, output, 'a');
    expect(output.a).to.be.deep.equals(undefined);
    output = {}
    _nested.resolve(rows[1], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expNest);
    output = {}
    _nested.resolve(rows[2], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expNest);
    output = {}

    _many.resolve(rows[0], {}, output, 'a');
    expect(output.a).to.be.deep.equals(undefined);
    output = {}
    _many.resolve(rows[1], {}, output, 'a');
    expect(output.a).to.be.deep.equals(undefined);
    output = {}
    _many.resolve(rows[2], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expMany);
    output = {}

    // modify a existing column to reference _many
    _nested.addField(_nested.fields.mnest.ref(_many).many);

    _nested.resolve(rows[0], {}, output, 'a');
    expect(output.a).to.be.deep.equals(undefined);
    output = {}
    _nested.resolve(rows[1], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expNest);
    output = {}
    _nested.resolve(rows[2], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expManyNest);
    output = {}

    // add a new column that references _nested
    _schema.addField({property: 'nested', isRef: true, reference: _nested});

    _schema.resolve(rows[0], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expBase);
    output = {}
    _schema.resolve(rows[1], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expBaseNest);
    output = {}
    _schema.resolve(rows[2], {}, output, 'a');
    expect(output.a).to.be.deep.equals(expBaseNestMany);
    output = {}
  });

  describe('Schema specific tests', () => {

    it('Should create the schema', function() {
      const src = 'SOURCE_NAME';
      const alias = 'AN_ALIAS';
      const cols = {
        name: 'name',
        createdAt: 'created_at'
      };

      // simple creation
      let schm = new Schema(src, alias, cols);
      expect(schm.alias).to.be.equals(alias);
      expect(schm.name).to.be.equals(src);
      expect(schm.size).to.be.equals(2);
      expect(schm.fields).to.have.keys(['name', 'createdAt']);

      // undefined alias should fallback to source
      schm = new Schema(src, undefined, cols);
      expect(schm.alias).to.be.equals(src);
      expect(schm.name).to.be.equals(src);

      // cols as alias should work
      schm = new Schema(src, cols);
      expect(schm.alias).to.be.equals(src);
      expect(schm.name).to.be.equals(src);

      // passing SOURCE and ALIAS as a cols property
      let _cols = {SOURCE: src, ALIAS: alias}
      schm = new Schema(_cols);
      expect(schm.alias).to.be.equals(alias);
      expect(schm.name).to.be.equals(src);

      // passing only SOURCE as a cols property
      _cols = {SOURCE: src}
      schm = new Schema(_cols);
      expect(schm.alias).to.be.equals(src);
      expect(schm.name).to.be.equals(src);
      expect(schm.size).to.be.equals(0);

    });

    it('Should validate the constructor parameters.', function() {
      const src = 'SOURCE_NAME';
      const alias = 'AN_ALIAS';
      const cols = {};

      // throw error if no source name is found
      const throwErrorNoSource = () => { new Schema(undefined, cols) };
      const throwErrorNoColSource = () => { new Schema(cols); }
      expect(throwErrorNoSource).to.throw(Error);
      expect(throwErrorNoColSource).to.throw(Error);

    });

    it('Should change the field alias when copying fields from other schemas', function() {
      const event = new Schema('EVENT', 'Event', {id: 'id', name: 'name', test: ''});
      expect(event.fields.id.alias).to.be.equals('Event_id');
      expect(event.fields.name.alias).to.be.equals('Event_name');

      const otherEvent = new Schema('EVENT', 'OtherEvent', event.fields);
      expect(otherEvent.fields.id.alias).to.be.equals('OtherEvent_id');
      expect(otherEvent.fields.name.alias).to.be.equals('OtherEvent_name');
    });
  });

  describe('Field specific tests', function() {
    let parent;

    beforeEach(() => {
      parent = new Schema('SCHEMA', 'SCHEMA', {});
    });

    it('Should throw error due invalid constructor parameters', function () {
      const missingParent = () => { new Field (null, 'property', 'field'); }
      const invalidProp1 = () => { new Field ({isParent: true}, '', 'field'); }
      const invalidProp2 = () => { new Field ({isParent: true}, undefined, 'field'); }
      const invalidFld1 = () => { new Field ({isParent: true}, 'prop', ''); }
      const invalidFld2 = () => { new Field ({isParent: true}, 'prop', undefined); }

      expect(missingParent).to.throw(Error);
      expect(invalidProp1).to.throw(Error);
      expect(invalidProp2).to.throw(Error);
      expect(invalidFld1).to.throw(Error);
      expect(invalidFld2).to.throw(Error);
    });

    it('Should create a new field from string', function() {
      const fld = new Field(parent, 'fld', 'testFieldName');

      expect(fld.isField).to.be.true;
      expect(fld.field).to.be.equals('testFieldName');
      expect(fld.isOwn).to.be.true;
      expect(fld.isRef).to.be.false;
      expect(fld.isKey).to.be.false;
    });

    it('Should create a ref field when its name resolves to false', function() {
      const fld1 = new Field(parent, 'fld1', '');
      const fld2 = new Field(parent, 'fld2', {field: false});
      const fld3 = new Field(parent, 'fld3', {});

      expect(fld1.field).to.not.be.ok;
      expect(fld1.isOwn).to.be.false;
      expect(fld1.isRef).to.be.true;

      expect(fld2.field).to.not.be.ok;
      expect(fld2.isOwn).to.be.false;
      expect(fld2.isRef).to.be.true;

      expect(fld3.field).to.not.be.ok;
      expect(fld3.isOwn).to.be.false;
      expect(fld3.isRef).to.be.true;
    });

    it('Should create a modified copy of itself when accessing the property "key"', function() {
      const fld = new Field(parent, 'fld', 'test');
      expect(fld.isOwn).to.be.true;
      expect(fld.isRef).to.be.false;
      expect(fld.isKey).to.be.false;

      const key = fld.key;
      expect(key.isOwn).to.be.false;
      expect(key.isRef).to.be.false;
      expect(key.isKey).to.be.true;

      expect(fld).to.not.be.equals(key);
      expect(fld.isKey).to.be.false;
    });

    it('Should create a modified copy of itself when accessing the property "many"', function() {
      const fld = new Field(parent, 'fld', 'test');
      expect(fld.isOwn).to.be.true;
      expect(fld.isRef).to.be.false;
      expect(fld.isKey).to.be.false;

      const many = fld.many;
      expect(many.isOwn).to.be.false;
      expect(many.isRef).to.be.true;
      expect(many.isKey).to.be.false;
      expect(many.isMany).to.be.true;
      expect(many.field).to.be.equals(undefined);

      expect(fld).to.not.be.equals(many);
      expect(fld.isKey).to.be.false;
    });

    it('Should create a modified copy of itself that references the schema passed as argument', function() {
      const fld = new Field(parent, 'fld', 'test');
      const fakeSchema = {isSchema: true};

      expect(fld.isOwn).to.be.true;
      expect(fld.isRef).to.be.false;
      expect(fld.isKey).to.be.false;

      const ref = fld.ref(fakeSchema);
      expect(ref.isOwn).to.be.false;
      expect(ref.isRef).to.be.true;
      expect(ref.isKey).to.be.false;
      expect(ref.isMany).to.be.false;
      expect(ref.field).to.be.equals(undefined);

      expect(fld).to.not.be.equals(ref);
      expect(fld.isKey).to.be.false;
    });

    it('Should throw error when acessing key property on fields that references other schemas', function () {
      const fld = new Field(parent, 'fld', {mapTo: {isSchema: true}});
      const errorOnKey = () => { fld.key };
      expect(errorOnKey).to.throw(Error);
    });

    it('Should assign value to context or throw error when passing wrong holder', function() {
      const one  = new Field(parent, 'one',  'test');
      const many = new Field(parent, 'many', {isMany: true});
      const context = {
        one: undefined,
        many: []
      }
      const val1 = 30;
      const val2 = 35;

      one.assignValue(context, val1);
      many.assignValue(context, val2);
      expect(context.one).to.be.equals(30);
      expect(context.many).to.be.deep.equals([35]);

      one.assignValue(context, val2);
      many.assignValue(context, val1);
      expect(context.one).to.be.equals(35);
      expect(context.many).to.be.deep.equals([35, 30]);

      const oneAsMany = one.many;
      const tryToPushOnNonArray = () => { oneAsMany.assignValue(context, 1) };
      expect(tryToPushOnNonArray).to.throw(Error);
    });
  });
});
