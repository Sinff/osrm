'use strict';

const expect = require('chai').expect;
const OSRM = require('../lib/osrm');
const KEY = 'test-osrm';

describe('Unit: ./lib/osrm', () => {
  var CTX, Q;

  beforeEach(() => {
    const params = {
      name: 'CTX',
      keywords: [{identifier: 'TEST'}],
      onCall: () => {},
      onMerge: () => {}
    };

    OSRM.register([params]);
    CTX = OSRM.CTX;
    Q = CTX();
  });

  it('Should modify the stash per context', function() {
    const context = OSRM.CTX();
    const first = context.getCurrentContext();
    const second = context.newContext();
    const s1 = first.retrieve(KEY);
    const s2 = second.retrieve(KEY);

    // All stash are created as an empty object
    expect(s1).to.be.deep.equals({isStash: true});
    expect(s2).to.be.deep.equals({isStash: true});
    expect(s2).to.be.equals(s2);
    expect(s2).to.not.be.equals(s1);

    // Data can be added for each independetly
    first.retrieve(KEY).data = 10;
    second.retrieve(KEY).data = 40;

    expect(s1).to.not.be.equals(10);
    expect(s2).to.not.be.equals(40);

    // Creating a new context should not modify the others
    const third = context.newContext();
    const s3 = third.retrieve(KEY);

    expect(s3).to.be.deep.equals({isStash: true});
    expect(s3.data).to.be.equals(undefined);
    expect(s1).to.not.be.equals(10);
    expect(s2).to.not.be.equals(40);

  });

  it('Root context should not have its own stash (Should use its children context)', function() {
    const context = OSRM.CTX();
    const first = context.getCurrentContext();
    const rs = context.retrieve(KEY);
    const s1 = first.retrieve(KEY);

    expect(rs).to.be.equals(s1);
    rs.data = 30;
    expect(s1.data).to.be.equals(rs.data);
    expect(s1.data).to.be.equals(30);

    s1.data2 = 40;
    expect(rs.data2).to.be.equals(s1.data2);
    expect(rs.data2).to.be.equals(40);
  });
});
