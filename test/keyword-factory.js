'use strict';

const expect = require('chai').expect;
const KeywordFactory = require('../lib/keyword-factory');

describe('Unit: ./lib/keyword-factory', () => {
  it('Should create the factory with null parameters', function() {
    const kw = KeywordFactory(null);
    expect(kw).to.have.keys('create');
  });

  it('Should create a new keyword with the given parameters', function () {
    const kw = KeywordFactory();
    const parser = () => {};
    const expected = {
      identifier: 'test',
      keySeparator: 'test',
      bodySeparator: 'test',
      callable: false,
      head: 'test',
      tail: 'test',
      merge: false,
      branch: false,
      parser: parser,
      onCall: () => {}
    };

    const keyword = kw.create(expected);
    expect(keyword).to.have.keys(Object.keys(expected));
  });

  it('Should throw error due missing name property', function () {
    const kw = KeywordFactory();
    const params = {name: undefined};
    expect(() => { kw.create(params); }).to.throw(Error);
  });

  it('Should throw error due a not allowed identifier property', function() {
    const params = {
      keywordProps: {
        identifier: 'this object cannot have a identifier'
      }
    };

    expect(() => { KeywordFactory(params); }).to.throw(Error);
  });

  it('Should clone the keyword with all properties', function() {
    const kf = KeywordFactory();
    const kw = kf.create({
      identifier: 'test',
      head: 'head (',
      tail: ')',
      on: {
        create: { handler: 'someHandler' },
        update: { tail: 'tail' }
      }
    });

    const cloned = kw.clone();
    expect(cloned).to.be.deep.equals(kw);

    // assures modifying the clone doesn't affect the original keyword
    cloned.on.create.a = 30;
    cloned.on.update.tail = 'new tail';
    expect(kw).to.not.be.deep.equals(cloned)
    expect(kw.on.create.a).to.be.not.ok;
    expect(kw.on.update.tail).to.be.equals('tail');
  });

  it('Should merge properties when creating a keyword with and existing identifier', function() {
    const kf = KeywordFactory();
    const kw = kf.create({
      identifier: 'IDENTIFIER',
      head: 'head (',
      tail: ')',
      on: {
        create: { handler: 'someHandler' },
        update: { tail: 'tail' }
      },
      other: 'to remove'
    });

    const modKw = kf.create({
      identifier: 'IDENTIFIER',
      head: 'new Head (',
      on: {
        delete: { handler: 'handler' },
        update: { tail: 'newTail', other: 30 }
      },
      other: undefined
    });

    const expected = {
      identifier: 'IDENTIFIER',
      head: 'new Head (',
      tail: ')',
      on: {
        delete: { handler: 'handler' },
        create: { handler: 'someHandler' },
        update: { tail: 'newTail', other: 30 }
      }
    };

    expect(Object.assign({}, modKw)).to.be.deep.equals(expected);
    expect(Object.assign({}, kw)).to.be.deep.equals(expected);
    expect(kw.keywords.IDENTIFIER).to.be.equals(modKw);
    expect(modKw).to.be.equals(kw);

  });

});
