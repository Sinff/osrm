'use strict';

const expect = require('chai').expect;
const KEY = 'test-osrm';
const Field = require('../sql/schema/schema-field');
const Schema = require('../sql/schema/schema');
const SchemaHandler = require('../sql/core/ctx-sch-handler');

describe('Unit: sql/schema-handler', function() {
  let USER, FRIENDS, ADDRESS, Handler;

  before(() => {
    USER = new Schema({
      SOURCE: 'USER',
      id: 'id',
      name: 'name',
      email: 'email',
      friends: null
    });

    FRIENDS = new Schema({
      SOURCE: 'USER',
      ALIAS: 'FRIENDS',
      id: 'id',
      name: 'name'
    });

    ADDRESS = new Schema({
      SOURCE: 'ADDRESS',
      id: 'id',
      route: 'route',
      postalCode: 'postal_code'
    });

    Handler = new SchemaHandler(Schema);
  });

  it('Should create a schema per field parent', function() {
    let context = {};
    const U = USER.fields, F = FRIENDS.fields, A = ADDRESS.fields;

    Handler.addFieldList(context, [U.id]);
    expect(context.schemas).to.have.keys(['USER']);

    context = {};
    Handler.addFieldList(context, [U.id, F.id]);
    expect(context.schemas).to.have.keys(['USER', 'FRIENDS']);

    context = {};
    Handler.addFieldList(context, [U.id, F.id, A.id]);
    expect(context.schemas).to.have.keys(['USER', 'FRIENDS', 'ADDRESS']);
  });

  it('Should add field reference', function() {
    let context = {};
    const expected = Object.keys(FRIENDS).concat(['isNested']);
    const U = USER.fields, F = FRIENDS.fields, A = ADDRESS.fields;

    Handler.addFieldList(context, [U.id, F.id]);
    Handler.addFieldRefs(context, FRIENDS, U.friends);
    expect(context.schemas).to.have.keys(['USER', 'FRIENDS']);

    const friends = Handler.getField(context, U.friends);
    expect(friends.reference).to.have.keys(expected);
    expect(friends.isRef).to.be.true;
  });

  it('Should throw error when adding references with invalid parameters type', function() {
    let context = {};

    expect(() => { Handler.addFieldRefs(context, USER, USER.fields.id) }).to.not.throw(Error);
    expect(() => { Handler.addFieldRefs(context, USER, 'invalid field') }).to.throw(Error);
    expect(() => { Handler.addFieldRefs(context, 'Invalid schema', USER.fields.id) }).to.throw(Error);
  });

  it('Should accept schemas as fields', function() {
    let context = {};
    const A = ADDRESS.fields;
    Handler.addFieldList(context, [USER, FRIENDS, A.id, A.route]);
    expect(context.schemas).to.have.keys(['USER', 'FRIENDS', 'ADDRESS']);
    expect(context.schemas[USER.alias].fields).to.have.keys(Object.keys(USER.fields));
    expect(context.schemas[USER.alias]).to.not.be.equals(USER);
  });

});
