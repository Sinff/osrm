'use strict';

const expect = require('chai').expect;
const CCF = require('../lib/context-class-factory');

describe('Unit: ./lib/context-class-factory', () => {
  it('Should create new ContextClass for each call', function() {
    const Ctx1 = CCF();
    const Ctx2 = CCF();
    const Ctx3 = Ctx2;

    expect(Ctx1).to.not.be.equals(Ctx2);
    expect(Ctx1.identifier).to.be.equals(Ctx2.identifier);
    expect(Ctx2).to.be.equals(Ctx3);
  });

  describe('Access/Create/Destroy contexts', function () {
    let context, first, nested, nested2, nested3, nested4;

    beforeEach(() => {
      const Ctx1 = CCF();
      context = new Ctx1();
      first = context.getCurrentContext();
      nested = first.newContext();
      nested2 = context.newContext();
      nested3 = nested.newContext();
      nested4 = nested3.newContext();
    });

    it('Should create nested contexts from any reference ', function() {
      expect(first.parent).to.be.equals(context);
      expect(first).to.not.be.equals(nested);
      expect(first).to.be.equals(nested.parent);
      expect(nested).to.not.be.equals(nested2);
      expect(nested).to.be.equals(nested2.parent);
      expect(nested3).to.be.equals(nested4.parent);
      expect(nested).to.be.equals(nested4.parent.parent.parent);
    });

    it('Should destroy the last context from any reference', function() {
      expect(context.getCurrentContext()).to.be.equals(nested4);
      context.destroyContext();
      expect(nested2.getCurrentContext()).to.be.equals(nested3);
      nested3.destroyContext();
      expect(first.getCurrentContext()).to.be.equals(nested2);
      first.destroyContext();
      expect(nested3.getCurrentContext()).to.be.equals(nested);
      nested4.destroyContext();
      expect(nested4.getCurrentContext()).to.be.equals(first);
      // Can't destroy the root context children
      expect(() => { context.destroyContext() }).to.throw(Error);
    });

    it('Should get the last context from any reference', function() {
      expect(context.context).to.be.equals(first);
      expect(context.getCurrentContext()).to.be.equals(nested4);
      expect(first.getCurrentContext()).to.be.equals(nested4);
      expect(nested.getCurrentContext()).to.be.equals(nested4);
      expect(nested2.getCurrentContext()).to.be.equals(nested4);
      expect(nested3.getCurrentContext()).to.be.equals(nested4);

      const last = context.newContext();
      expect(first.getCurrentContext()).to.be.equals(last);
      expect(nested3.getCurrentContext()).to.be.equals(last);

    });

    it('Should get the root context from any reference', function() {
      expect(context.getRootContext()).to.be.equals(context);
      expect(first.getRootContext()).to.be.equals(context);
      expect(nested.getRootContext()).to.be.equals(context);
      expect(nested2.getRootContext()).to.be.equals(context);
      expect(nested3.getRootContext()).to.be.equals(context);
    });

    it('Should create proxied contexts from any reference. They should not be nested', function() {
      const proxy1 = context.proxyContext();
      expect(context.getCurrentContext()).to.be.equals(nested4);
      expect(proxy1.parent).to.be.equals(context);

      const proxy2 = nested3.proxyContext();
      expect(nested3.getCurrentContext()).to.be.equals(nested4);
      expect(proxy2.parent).to.be.equals(context);
    });
  });
});
