'use strict';

const expect = require('chai').expect;
const CCF = require('../lib/context-class-factory');
const KEY = 'sql-plugin';
const OSRM = require('../index');

describe('Integration: OSRM with the default SQLPlugin', () => {
  var SQL, Q, osrm;

  beforeEach(() => {
    osrm = OSRM.create({exportsAsRoot: false});
    SQL = osrm.SQL;
    Q = SQL();
  });

  describe('Plugin creation', function(){
    it('Should ignore properties not registered on Field', function(){
      SQL.schema({
        SOURCE: 'TEST',
        prop: { field: 'test', invalidPropertyName: 10, isKey: true }
      });

      expect(SQL.TEST().prop.invalidPropertyName).to.not.be.ok;
      expect(SQL.TEST().prop.isKey).to.be.true;
    });

    it('Should allow field properties customization', function(){
      const p = new OSRM.SQLPlugin({customFieldProps: ['newPropName']});
      OSRM.register(p);
      const osrm = OSRM.create({exportsAsRoot: false});

      osrm.SQL.schema({
        SOURCE: 'TEST',
        prop: { field: 'test', newPropName: 10, isKey: true }
      });
      osrm.SQL.schema({
        SOURCE: 'OTHER',
        prop: 'test'
      });

      expect(osrm.SQL.TEST().prop.newPropName).to.be.equals(10);
      expect(osrm.SQL.TEST().prop.isKey).to.be.true;
      expect(osrm.SQL.OTHER().prop.newPropName).to.not.be.ok;
      expect(osrm.SQL.OTHER().prop.isKey).to.be.false;
    });

    it('Should expose parsers as field props', function(){
      const fieldParsers = [
        {
          name: 'num',
          parseObj: (v) => {
            return Number(v);
          }
        },
        {
          name: 'date',
          parseObj: (v) => {
            return new Date(v);
          }
        }
      ]

      const plugin = new OSRM.SQLPlugin({objParsers: fieldParsers});
      OSRM.register(plugin);
      const osrm = OSRM.create({exportsAsRoot: false});
      SQL = osrm.SQL;


      SQL.schema({
        SOURCE: 'PARSERS',
        ALIAS: 'Parsers',
        numVal:   {field: 'num_val',   parser: 'num'  },
        dateVal:  {field: 'date_val',  parser: 'date' },
        otherVal: {field: 'other_val'                 }
      });

      const PS = SQL.Parsers();

      expect(PS.numVal.parse).to.be.a('function');
      expect(PS.dateVal.parse).to.be.a('function');
      expect(PS.otherVal.parse).to.not.be.ok;

      let ctx = {};
      PS.numVal.assignValue(ctx, '35');
      expect(ctx.numVal).to.be.a('number');
      expect(ctx.numVal).to.be.equals(35);

      ctx = {};
      PS.dateVal.assignValue(ctx, 1472166983734);
      expect(ctx.dateVal).to.be.a('date');
      expect(ctx.dateVal.getTime()).to.be.equals(1472166983734);

      //Should not modify the value once there is no parser
      ctx = {};
      PS.otherVal.assignValue(ctx, {});
      expect(ctx.otherVal).to.be.a('object');
      expect(ctx.otherVal).to.be.deep.equals({});

      ctx = {};
      PS.otherVal.assignValue(ctx, '3');
      expect(ctx.otherVal).to.be.a('string');
      expect(ctx.otherVal).to.be.equals('3');

      PS.otherVal.assignValue(ctx, 1472167983734);
      expect(ctx.otherVal).to.be.a('number');
      expect(ctx.otherVal).to.be.equals(1472167983734);

      // Should modify the otherVal to have a number parser
      ctx = {};
      PS.otherVal = PS.otherVal.num;
      PS.otherVal.assignValue(ctx, '3');
      expect(ctx.otherVal).to.be.a('number');
      expect(ctx.otherVal).to.be.equals(3);

      // Should modifiy the otherVal to have a date parser
      ctx = {};
      PS.otherVal = PS.otherVal.date;
      PS.otherVal.assignValue(ctx, 1472167983734);
      expect(ctx.otherVal).to.be.a('date');
      expect(ctx.otherVal.getTime()).to.be.equals(1472167983734);
    });
  });

  describe('Schema creation', function () {

    it('Should expose the schema by its alias', function() {
      SQL.schema({
        SOURCE: 'EVENT',
        ALIAS:  'Event'
      });

      expect(SQL.EVENT).to.not.be.ok;
      expect(SQL.Event).to.be.ok;
    });

    it('Should expose the schema by its SOURCE name when no alias was given', function() {
      SQL.schema({
        SOURCE: 'EVENT'
      });

      expect(SQL.Event).to.not.be.ok;
      expect(SQL.EVENT).to.be.ok;
    });

    it('Should expose a extensible schema', function() {
      SQL.schema({
        SOURCE: 'EVENT',
        id:       { field: 'id', isKey:  true },
        contacts: { field: ''  , isMany: true },
        name:     'name',
        email:    'email',
      });

      const EVENT = SQL.EVENT();
      expect(EVENT).to.have.keys(['id', 'name', 'email', 'contacts']);

      const E1 = SQL.EVENT({other: 'other'});
      expect(E1).to.have.keys(['id', 'name', 'email', 'contacts', 'other']);
      expect(EVENT).to.have.keys(['id', 'name', 'email', 'contacts']);
    });

    it('Should allow alias customization.', function() {
      SQL.schema({
        SOURCE: 'ONE', // no alias
        test:   'test',
      });

      SQL.schema({
        SOURCE: 'TWO',
        ALIAS:  'two', // lowercase alias
        test:   'test',
      });

      const E1 = SQL.ONE();
      const oneEvt = SQL.toSql(SQL() .FROM (E1));
      expect(oneEvt).to.be.equals('FROM ONE AS ONE');

      const E2 = SQL.ONE('customAlias');
      const twoEvt = SQL.toSql(SQL() .FROM (E2));
      expect(twoEvt).to.be.equals('FROM ONE AS customAlias');

      const E3 = SQL.two();
      const thrEvt = SQL.toSql(SQL() .FROM (E3));
      expect(thrEvt).to.be.equals('FROM TWO AS two');

      const E4 = SQL.two('customAliasTwo');
      const fouEvt = SQL.toSql(SQL() .FROM (E4));
      expect(fouEvt).to.be.equals('FROM TWO AS customAliasTwo');

    });
  });

  describe('Query creation', function () {

    it('Should build the sql on keyword calls' , function() {
      let q = Q.context;
      const stash = q.retrieve(KEY);
      expect(stash.sql).to.be.equals(undefined);
      q.SELECT('column');
      expect(stash.sql).to.be.equals("SELECT 'column'");
      q.FROM('Table');
      expect(stash.sql).to.be.equals("SELECT 'column' FROM 'Table'");
      q.WHERE;
      expect(stash.sql).to.be.equals("SELECT 'column' FROM 'Table' WHERE");
      q.EQUALS(10, 10);
      expect(stash.sql).to.be.equals("SELECT 'column' FROM 'Table' WHERE 10=10");
    });

    it('Should create two diferent objects when calling SQL', function() {
      const Q2 = SQL();
      expect(Q2).to.have.keys(Object.keys(Q));
      expect(Q2).to.not.be.equals(Q);
      expect(Q.retrieve(KEY)).to.be.deep.equals(Q2.retrieve(KEY));
      Q.SELECT('a');
      expect(Q.retrieve(KEY)).to.not.be.deep.equals(Q2.retrieve(KEY));
    });

    it('Should work with callable references', function() {
      const SELECT = Q.SELECT;
      const FROM = Q.FROM;

      // creating references should not modify the sql
      expect(Q.retrieve(KEY).sql).to.be.equals(undefined);

      SELECT ('column1', 'column2'); FROM ('TESTE');
      expect(Q.retrieve(KEY).sql).to.be.equals("SELECT 'column1','column2' FROM 'TESTE'");
    });

    it('Should work with non callable references', function() {
      const WHERE = Q.WHERE;
      const AND = Q.AND;

      // creating references should not modify the sql
      expect(Q.retrieve(KEY).sql).to.be.equals(undefined);

      Q.SELECT ('column1', 'column2') .FROM ('TESTE');
      WHERE .EQUALS('10', '10') .AND .EQUALS(15, 15);
      expect(Q.retrieve(KEY).sql).to.be.equals("SELECT 'column1','column2' FROM 'TESTE' WHERE '10'='10' AND 15=15");
    });
/*
    it('Should create scoped queries', function () {
      const expected = "SELECT 'column1','column2' FROM 'TESTE' WHERE ( 'column3'=3 AND 'column5'=2 ) AND 'column3'=7";

      Q
      .SELECT ('column1', 'column2') .FROM ('TESTE')
      .WHERE
      .SCOPE
      .EQUALS ( 'column3', 3 ) .AND
      .EQUALS ( 'column5', 2 )
      .END
      .AND .EQUALS ( 'column3', 7 )

      expect(Q.retrieve(KEY).sql).to.be.equals(expected);
    });

    it('Should create nested scoped queries', function () {
      const expected = "SELECT 'column1','column2' FROM 'TESTE' WHERE ( 'column3'=3 AND 'column5'=2 AND ( 'column9'=5 AND 'column10'=16 ) ) AND 'column3'=7";
      Q
      .SELECT ('column1', 'column2') .FROM ('TESTE')
      .WHERE
      .SCOPE
      .EQUALS ( 'column3', 3 ) .AND
      .EQUALS ( 'column5', 2 ) .AND
      .SCOPE
      .EQUALS('column9', 5) .AND
      .EQUALS('column10', 16)
      .END
      .END
      .AND .EQUALS ( 'column3', 7 )

      expect(Q.retrieve(KEY).sql).to.be.equals(expected);
    });

    it('Should throw error when merging without creating a branch', function() {
      const throwsError  = () => { SQL().SELECT('a').FROM('b').END };
      const throwsError2 = () => { SQL().SELECT('a').FROM('b').SCOPE.EQUALS(1,1).END.END };
      const notThrow = () => { SQL().SELECT('a').FROM('b').SCOPE.EQUALS(1,1).END };
      expect( throwsError  ).to.throw(Error);
      expect( throwsError2 ).to.throw(Error);
      expect( notThrow ).to.not.throw(Error);
    });

    it('Should allows keyword references', function() {
      const expected = "SELECT 'column1','column2' FROM 'TESTE' WHERE ( 'column3'=3 AND 'column5'=2 AND ( 'column9'=5 AND 'column10'=16 ) ) AND 'column3'=7";
      const SCOPE = Q.SCOPE;
      const END = Q.END;
      const SELECT = Q.SELECT;
      const WHERE = Q.WHERE;
      const EQUALS = Q.EQUALS;
      const AND = Q.AND;

      // The following code was done for test purpose, it is not recommended to
      // mix so many references and chained calls.
      SELECT ('column1', 'column2') .FROM ('TESTE')
      WHERE
      .SCOPE
      EQUALS ( 'column3', 3 )
      AND .EQUALS ( 'column5', 2 ) .AND
      SCOPE
      .EQUALS ( 'column9', 5 ) .AND
      EQUALS ( 'column10', 16 ) .END
      END
      .AND
      .EQUALS ( 'column3', 7 );

      expect(Q.retrieve(KEY).sql).to.be.equals(expected);
    });

    it('Should allows other queries as parameters', function() {
      const expected = "SELECT 'a',( SELECT 'j' ),'u'";
      const SCP = SQL().SCOPE;
      Q .SELECT (
        'a',
        SCP .SELECT( 'j' ) .END,
        'u'
      );
      expect(Q.retrieve(KEY).sql).to.be.equals(expected);
    });
*/
    it('Should accept array as arguments on "IN" keywords', function() {
      const expected = '\'teste\' IN ( 1,2,3,4,5,6 )';
      Q .COL('teste') .IN (1,2,3,4, [5,6]);
      expect(Q.retrieve(KEY).sql).to.be.equals(expected);
    });
  });
});
