'use strict';

const expect = require('chai').expect;
const _OSRM = require('../index');

describe('Integration: osrm', function() {
  let OSRM, SQL;

  beforeEach(() => {
    OSRM = _OSRM.create({exportsAsRoot: false});
    SQL = OSRM.SQL;

    SQL.schema({
      SOURCE: 'USER', // The real table name
      ALIAS: 'User', // The exposed object name e.g. SQL.User
      id: { field: 'id', isKey: true },
      name: 'name',
      birthdate: 'birthdate',
      createdAt: 'created_at',
      orders: '', // empty string becomes a reference,
      products: ''
    });

    SQL.schema({
      SOURCE: 'ORDER',
      ALIAS: 'Order',
      userId: 'user_id',
      productId: 'product_id',
      date: 'date',
      total: '' // Doesn't have mapping to database
    });

    SQL.schema({
      SOURCE: 'PRODUCT',
      ALIAS: 'Product',
      id: 'id',
      name: 'name',
      price: 'price',
      tags: ''
    });

    SQL.schema({
      SOURCE: 'TAG',
      ALIAS: 'Tag',
      id: 'id',
      description: 'description',
      productId: 'product_id'
    });
  });

  it('Should query many to many ignoring intermediary table', function() {
    const Q = SQL();
    const P = SQL.Product();
    const U = SQL.User();
    const O = SQL.Order();
    const rows = [
      {Product_name: 'Plutonium', User_id: 1},
      {Product_name: 'Cesium', User_id: 1},
      {Product_name: 'Uranium', User_id: 1},
      {Product_name: 'Cesium', User_id: 2},
      {Product_name: 'Uranium', User_id: 2},
    ];

    const expectedQue = 'SELECT Product.name AS Product_name,User.id AS User_id FROM USER AS User INNER JOIN ORDER AS Order ON Order.user_id=User.id INNER JOIN PRODUCT AS Product ON Product.id=Order.product_id';
    const expectedObj = {
      User: [
        {id: 1, products: [
          {name: 'Plutonium' },
          {name: 'Cesium' },
          {name: 'Uranium' }
        ]},
        {id: 2, products: [
          {name: 'Cesium' },
          {name: 'Uranium' }
        ]
      }]
    };

    const ctx = Q
    .SELECT (P.name.key, U.id) .FROM(U)
      .INNER .JOIN ( O ) .ON .EQUALS( O.userId, U.id )
      .INNER .JOIN ( P, U.products ) .ON .EQUALS ( P.id, O.productId );


    expect(SQL.toSql(ctx)).to.be.equals(expectedQue);
    expect(SQL.toObject(ctx, rows)).to.have.keys(['User']);

  });

  it('Should query a single user', function() {
    const Q = SQL();
    const U = SQL.User();
    const ctx = Q. SELECT (U.id, U.name, U.birthdate, U.createdAt) .FROM ( U );
    //const ctx = Q. SELECT (U.id) .FROM ( U );
    const expectedQue = "SELECT User.id AS User_id,User.name AS User_name,User.birthdate AS User_birthdate,User.created_at AS User_created_at FROM USER AS User";
    const expectedObj = { id: 1, name: 'Stewie', birthdate: '05/06/2005 07:34:42', createdAt: '22/08/2016 16:03:00' };
    const rows = [{User_id: 1, User_name: 'Stewie', User_birthdate: '05/06/2005 07:34:42', User_created_at: '22/08/2016 16:03:00'}];

    expect(SQL.toSql(ctx)).to.be.equals(expectedQue);
    expect(SQL.toObject(ctx, rows)).to.have.keys(['User']);
    expect(SQL.toObject(ctx, rows).User[0]).to.be.deep.equals(expectedObj);
  });

  it('Should query the user discarding duplicated', function() {
    const Q = SQL();
    const U = SQL.User();
    const ctx = Q. SELECT (U.id, U.name, U.birthdate, U.createdAt) .FROM ( U );
    const expectedQue = "SELECT User.id AS User_id,User.name AS User_name,User.birthdate AS User_birthdate,User.created_at AS User_created_at FROM USER AS User";
    const expectedObj = { id: 1, name: 'Stewie', birthdate: '05/06/2005 07:34:42', createdAt: '22/08/2016 16:03:00' };
    const rows = [
      {User_id: 1, User_name: 'Stewie', User_birthdate: '05/06/2005 07:34:42', User_created_at: '22/08/2016 16:03:00'},
      {User_id: 1, User_name: 'Stewie', User_birthdate: '05/06/2005 07:34:42', User_created_at: '22/08/2016 16:03:00'}
    ];

    expect(SQL.toSql(ctx)).to.be.equals(expectedQue);
    expect(SQL.toObject(ctx, rows)).to.have.keys(['User']);
    expect(SQL.toObject(ctx, rows).User.length).to.be.equals(1);
    expect(SQL.toObject(ctx, rows).User[0]).to.be.deep.equals(expectedObj);
  });

  it('Should query two unrelated schemas', function() {
    const Q = SQL();
    const U = SQL.User();
    const T = SQL.Tag();
    const rows = [
      {User_name: 'Stewie', Tag_id: 1, Tag_description: 'Pu'},
      {User_name: 'Peter',  Tag_id: 3,  Tag_description: 'Cs'},
      {User_name: 'Stewie',  Tag_id: 3,  Tag_description: 'Cs'},
      {User_name: 'Peter', Tag_id: 1, Tag_description: 'Pu'}
    ];

    const expectedObj = {
      User: [
        {name: 'Stewie'},
        {name: 'Peter'}
      ],
      Tag: [
        {id: 1, description: 'Pu'},
        {id: 3, description: 'Cs'}
      ]
    };

    const ctx = Q
      .SELECT (U.name.key, T.id.key, T.description) .FROM ( U )
      .INNER .JOIN ( T ) .ON .EQUALS (T.id, 3) .OR .EQUALS (T.id, 1)
      .WHERE .EQUALS (U.name, 'Stewie') .OR .EQUALS (U.name, 'Peter');

    const expectedQue = "SELECT User.name AS User_name,Tag.id AS Tag_id,Tag.description AS Tag_description FROM USER AS User INNER JOIN TAG AS Tag ON Tag.id=3 OR Tag.id=1 WHERE User.name='Stewie' OR User.name='Peter'";

    expect(SQL.toSql(ctx)).to.be.equals(expectedQue);
    const output = SQL.toObject(ctx, rows);
    expect(output).to.have.keys(['Tag', 'User']);
    expect(output).to.be.deep.equals(expectedObj);
  });

  it('Should query PRODUCT with nested TAG in random order', function() {
    const Q = SQL();
    const P = SQL.Product();
    const T = SQL.Tag();
    const rows = [
      {Product_name: 'Plutonium', Product_price: '$$$',   Tag_id: 1,    Tag_description: 'Pu'},
      {Product_name: 'Cesium',    Product_price: '$$$$',  Tag_id: 5,    Tag_description: 'Cs'},
      {Product_name: 'Uranium',   Product_price: '$$',    Tag_id: 3,    Tag_description: 'U'},
      {Product_name: 'Plutonium', Product_price: '$$$',   Tag_id: 4,    Tag_description: 'Expensive'},
      {Product_name: 'Uranium',   Product_price: '$$',    Tag_id: 7,    Tag_description: 'Dangerous'},
      {Product_name: 'Plutonium', Product_price: '$$$',   Tag_id: 2,    Tag_description: 'Dangerous'},
      {Product_name: 'Cesium',    Product_price: '$$$$',  Tag_id: 13,   Tag_description: 'Chemical'},
      {Product_name: 'Plutonium', Product_price: '$$$',   Tag_id: 6,    Tag_description: 'Chemical'},
      {Product_name: 'Cesium',    Product_price: '$$$$',  Tag_id: 21,   Tag_description: 'Dangerous'},
      {Product_name: 'Uranium',   Product_price: '$$',    Tag_id: 8,    Tag_description: 'Chemical'},
    ];

    const expectedObj = {
      Product: [
        {name: 'Plutonium', price: '$$$', tags: [
          {id: 1, description: 'Pu'},
          {id: 4, description: 'Expensive'},
          {id: 2, description: 'Dangerous'},
          {id: 6, description: 'Chemical'},
        ]},
        {name: 'Cesium', price: '$$$$', tags: [
          {id: 5, description: 'Cs'},
          {id: 13, description: 'Chemical'},
          {id: 21, description: 'Dangerous'},
        ]},
        {name: 'Uranium', price: '$$', tags: [
          {id: 3, description: 'U'},
          {id: 7, description: 'Dangerous'},
          {id: 8, description: 'Chemical'},
        ]}
      ]
    };

    const ctx = Q
      .SELECT (P.name.key, P.price, T.id.key, T.description) .FROM ( P )
      .INNER .JOIN (T, P.tags.many) .ON .EQUALS (T.productId, P.id);

    const expectedQue = "SELECT Product.name AS Product_name,Product.price AS Product_price,Tag.id AS Tag_id,Tag.description AS Tag_description FROM PRODUCT AS Product INNER JOIN TAG AS Tag ON Tag.product_id=Product.id";

    const output = SQL.toObject(ctx, rows);
    expect(SQL.toSql(ctx)).to.be.equals(expectedQue);
    expect(output).to.have.keys(['Product']);
    expect(output).to.be.deep.equals(expectedObj);
  });

  it('Should throw error due invalid number of arguments', function() {
    const O = SQL.Order();
    const Q = SQL();

    expect( () => {Q.INNER .JOIN(O, O.userId)}).to.not.throw(Error);
    expect( () => {Q.INNER .JOIN(O, O.userId, 3)}).to.throw(Error);
    expect( () => {Q.SELECT()} ).to.not.throw(Error);
  });

  it('Should insert values', function() {
    SQL.schema({
      SOURCE: 'EVENT',
      ALIAS:  'Event',
      name: 'name',
      description: 'description',
      date: 'date'
    });
    const E = SQL.Event();
    const expectedQue = 'INSERT INTO EVENT ( name,description ) VALUES ( \'test\',\'desct\' )';
    const Q = SQL();
    Q.
      INSERT.INTO ( E ) .
      COLS (E.name, E.description) .
      VALUES ( 'test', 'desct' );

    expect(SQL.toSql(Q)).to.be.equals(expectedQue);
  });

  it('Should insert multi values', function() {
    SQL.schema({
      SOURCE: 'EVENT',
      ALIAS:  'Event',
      name: 'name',
      description: 'description',
      date: 'date'
    });
    const E = SQL.Event();
    const expectedQue = 'INSERT INTO EVENT ( description,name ) VALUES ( \'asdasd\',\'name 1\' ),( \'dsadsa\',\'name 2\' ),( \'3s3d3a\',\'name 3\' )';
    const obj1 = { name: 'name 1', description: 'asdasd' };
    const obj2 = { name: 'name 2', description: 'dsadsa' };
    const obj3 = { name: 'name 3', description: '3s3d3a' };
    const Q = SQL();
    Q
      .INSERT.INTO ( E )
      .VALUES ( obj1 )
      .VALUES ( obj2 )
      .VALUES ( obj3 )

    expect(SQL.toSql(Q)).to.be.equals(expectedQue);

  });

  it('Should insert values from an object', function() {
    SQL.schema({
      SOURCE: 'EVENT',
      ALIAS:  'Event',
      name: 'name',
      description: 'description'
    });
    const E = SQL.Event();
    const expectedQue = 'INSERT INTO EVENT ( description,name ) VALUES ( \'event description\',\'test\' )';
    const Q = SQL();

    Q.INSERT .INTO (E) .VALUES({name: 'test', description: 'event description', invalid: 'not picked property'});
    expect(SQL.toSql(Q)).to.be.equals(expectedQue);
  });

  it('Should insert values merging dummy fields with same name', function() {
    SQL.schema({
      SOURCE: 'EVENT',
      ALIAS:  'Event',
      name: 'name',
      description: 'description',
      dummy1: {field: 'x', isDummy: true},
      dummy2: {field: 'x', isDummy: true}
    });
    const E = SQL.Event();
    let expectedQue = 'INSERT INTO EVENT ( description,name,x ) VALUES ( \'event description\',\'test\',\'dummy1\',\'dummy2\' )';
    let Q = SQL();

    Q.INSERT.INTO (E) .VALUES({description: 'event description', name: 'test', invalid: 'not picked property', dummy1: 'dummy1', dummy2: 'dummy2'});
    expect(SQL.toSql(Q)).to.be.equals(expectedQue);

    // Should let only the last dummy on cols
    Q = SQL();
    Q.INSERT.INTO (E) .COLS (E.name, E.dummy1, E.description, E.dummy2) .VALUES('10', '20', '30', '40');
    expectedQue = 'INSERT INTO EVENT ( name,description,x ) VALUES ( \'10\',\'20\',\'30\',\'40\' )';
    expect(SQL.toSql(Q)).to.be.equals(expectedQue);
  });

  it('Should filter the object properties by the schema fields and vice versa', function() {
    SQL.schema({
      SOURCE: 'EVENT',
      ALIAS:  'Event',
      name: 'name',
      description: 'description',
      date: 'date'
    });

    let expectedQue = 'INSERT INTO EVENT ( date,description,name ) VALUES ( \'another valid one\',\'a valid one\',\'test\' )';
    let inputObj = {
      name: 'test',
      ignoredProperty: 'this should be ignored once it is not on schema',
      otherIgnored: 'this too',
      description: 'a valid one',
      date: 'another valid one'
    };

    const E = SQL.Event();

    let Q = SQL();
    Q.INSERT.INTO (E) .VALUES(inputObj);
    expect(SQL.toSql(Q)).to.be.equals(expectedQue);

    // If there is more fields than object properties declared, the extra fields should be ignored.
    Q = SQL();
    inputObj = { name: 'test', invalidOne: 'invalid' };
    expectedQue = 'INSERT INTO EVENT ( name ) VALUES ( \'test\' )';
    Q.INSERT.INTO (E) .VALUES(inputObj);
    expect(SQL.toSql(Q)).to.be.equals(expectedQue);
  });

  it('Should call the custom parse with meta data when the field isn\'t raw', function() {
    const parser = {
      canParseSql: (kw, val) => {
        return val.props && (val.props.custom || val.props.composed || val.props.ignore);
      },
      parseSql: (kw, val, ctx) => {
        if(val.props.custom) {
          return '\'new value\'';

        } else if (val.props.composed && !ctx.composed) {
          ctx.composed = true;
          ctx.data1 = val.value;
          return;

        } else if (val.props.ignore) {
          return;

        } else if (ctx.composed) {
          return  '\'' + ctx.data1 + val.value + '\'';
        }
      }
    }

    const plugin = new _OSRM.SQLPlugin({customFieldProps: ['custom', 'composed', 'ignore'], sqlParsers: [parser]})

    _OSRM.register(plugin);
    const osrm = _OSRM.create({exportsAsRoot: false});
    SQL = OSRM.SQL;

    SQL.schema({
      SOURCE: 'EVENT',
      ALIAS:  'Event',
      name: {field: 'name', custom: true},
      description: 'description',
      dummy1: {field: 'x', isDummy: true, composed: true},
      value1: {field: 'j', ignore: true},
      dummy2: {field: 'x', isDummy: true, composed: true},
      value2: {field: 'k', ignore: true}
    });

    const E = SQL.Event();
    let expectedQue = 'INSERT INTO EVENT ( description,j,k,name,x ) VALUES ( \'event description\',\'new value\',\'dummy1dummy2\' )';
    let Q = SQL();

    Q.INSERT.INTO (E) .VALUES({description: 'event description', name: 'test', invalid: 'not picked property', dummy1: 'dummy1', dummy2: 'dummy2', value1: 13, value2: 'ignored'});
    expect(SQL.toSql(Q)).to.be.equals(expectedQue);

    // The same as before but now specifying the columns
    Q = SQL();
    Q.INSERT.INTO (E)
    .COLS (E.description, E.value1, E.value2, E.name, E.dummy1, E.dummy2)
    .VALUES('event description', 13, 'test', 'not picked property','dummy1', 'dummy2');
    expect(SQL.toSql(Q)).to.be.equals(expectedQue);

  });

  it('Should map fields with AS keyword', function() {
    let U = SQL.User({count: 'count'});
    let qry = SQL();
    let SELECT = qry.SELECT;
    let rows = [{'User_count': 35}];

      SELECT () .COUNT(U.name) .AS ( U.count.key ) .FROM( U )

    expect(SQL.toSql(qry)).to.be.equals('SELECT COUNT(User.name) AS User_count FROM USER AS User');
    expect(SQL.toObject(qry, rows).User[0]).to.be.deep.equals({count: 35});

  });

  it('Should not put a bodySeparator when the keyword is invisible.', function() {
    SQL.schema({
      SOURCE             : 'CREDENTIALS',
      ALIAS              : 'Credentials',
      // FIELDS
      id                 : {field: 'id', isKey: true},
      // INVISIBLE PROPS
      invisibleOne       : { field: 'invisible_one', isVisible: false },
      invisibleTwo       : { field: 'invisible_two', isVisible: false }
    });

    const Cr = SQL.Credentials();
    const qry = SQL();
    const SELECT = qry.SELECT;
    const FROM = qry.FROM;

      SELECT ( Cr )
      FROM ( Cr )

    expect( SQL.toSql(qry) ).to.be.equals('SELECT Credentials.id AS Credentials_id FROM CREDENTIALS AS Credentials');
  });

  it('Should turn the field visible when calling "show"', function() {
    SQL.schema({
      SOURCE             : 'USER',
      ALIAS              : 'User',
      // FIELDS
      id                 : {field: 'id', isKey: true},
      name               : 'name',
      // INVISIBLE PROPS
      password           : { field: 'password', isVisible: false },
      username           : { field: 'username', isVisible: false }
    });

    const Us = SQL.User();
    let qry = SQL();
    let SELECT = qry.SELECT;
    let FROM = qry.FROM;

      SELECT ( Us )
      FROM ( Us )

    expect( SQL.toSql(qry) ).to.be.equals('SELECT User.id AS User_id,User.name AS User_name FROM USER AS User');
    qry = SQL();
    SELECT = qry.SELECT;
    FROM = qry.FROM;

    SELECT ( Us, Us.password.show, Us.username.show )
    FROM ( Us )

    expect( SQL.toSql(qry) ).to.be.equals('SELECT User.id AS User_id,User.name AS User_name,User.password AS User_password,User.username AS User_username FROM USER AS User');
  });

  it('Should UPDATE the table with ASSIGN keyword', function() {
    const Us = SQL.User();
    let qry = SQL();

    qry.
      UPDATE (Us). SET.
      ASSIGN(Us.name, 'test name').
      ASSIGN(Us.id, 33).
      WHERE. EQUALS ( Us.id, 37 );

    expect( SQL.toSql(qry) ).to.be.equals('UPDATE USER SET name=\'test name\',id=33 WHERE id=37');
  });

  it('Should UPDATE the table with VALUES keyword', function() {
    const Us = SQL.User();
    let qry = SQL();

    qry.
      UPDATE (Us). SET.
      VALUES ({name: 'test name', id: 33, birthdate: 12312312312, createdAt: 12312312312, nonExistentProp: 35, orders: 123123 }).
      WHERE. EQUALS (Us.id, 37);

    expect( SQL.toSql(qry) ).to.be.equals('UPDATE USER SET birthdate=12312312312,created_at=12312312312,id=33,name=\'test name\' WHERE id=37');
  });

  it('Should DELETE the table', function() {
    const Us = SQL.User();
    let qry = SQL();

    qry.
      DELETE. FROM(Us). WHERE. EQUALS (Us.id, 37);

    expect ( SQL.toSql(qry) ).to.be.equals('DELETE FROM USER WHERE id=37');
  });

  it('Should combine COL and AS to map columns to other alias', function() {
    const Us = SQL.User();
    const expected = 'SELECT User.id AS User_id,User.name AS User_id FROM USER AS User';
    let qry = SQL();

    qry.
      SELECT (Us.id) .COL(Us.name).AS(Us.id) .FROM (Us);

    expect( SQL.toSql(qry) ).to.be.equals(expected);
  });

  it('Should reference the field by its alias when AS is called after a function keyword', function() {
    const Us = SQL.User({n1: 'n1', n2: 'n2'});
    const expected = 'SELECT COUNT(User.id) AS User_n1,MAX(User.name) AS User_n2 FROM USER AS User WHERE User_n1=User_n2';
    const expected2 = 'SELECT User.id AS User_n1,MAX(User.name) AS User_n2 FROM USER AS User WHERE User.n1=User_n2';
    let qry = SQL();

    // The query validity was not considered here
    qry. SELECT () .COUNT(Us.id) .AS(Us.n1) .MAX(Us.name) .AS(Us.n2) .FROM (Us) .WHERE .EQUALS(Us.n1, Us.n2);

    expect( SQL.toSql(qry) ).to.be.equals(expected);

    qry = SQL();
    qry .SELECT () .COL(Us.id) .AS(Us.n1) .MAX(Us.name) .AS(Us.n2) .FROM (Us) .WHERE .EQUALS(Us.n1, Us.n2);

    expect( SQL.toSql(qry) ).to.be.equals(expected2);
  });

});
