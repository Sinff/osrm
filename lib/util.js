'use strict';

const util = {};
const STRING = 'string';

util.toKeywordArray = function(keywords) {
  const arr = []
  let currKeyword;

  Object.keys(keywords).forEach((key) => {
    currKeyword = keywords[key];
    currKeyword.keyword = key;
    arr.push(currKeyword);
  });

  return arr;
};

util.assert = function ( expectation, errorMessage ) {
  if(!expectation) {
    throw new Error(errorMessage);
  }
};

util.isOk = function (val) {
  return (val !== undefined && val !== null);
}

util.isObject = function (val) {
  return (typeof val === 'object' && !Array.isArray(val));
}

util.isNonEmptyString = function(val) {
  return (typeof val === 'string' && val !== '');
}

util.isNonEmptyArray = function(val) {
  return Array.isArray(val) && val.length > 0;
}

util.isString = function(val) {
  return typeof val === STRING;
}

util.propertiesAsArray = function (object, propName) {
  let arr = [];

  Object.keys(object).forEach((key) => {
    if( util.isNonEmptyString(propName) && util.isObject(object[key])) {
      object[key][propName] = key;
    }
    arr.push(object[key]);
  });

  return arr;
}

util.isFunction = function(func) {
  return typeof func === 'function';
}

module.exports = util;
