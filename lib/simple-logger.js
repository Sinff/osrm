'use strict';

// Just a simple console logger for debugging purpose

var VERBOSE = 0;
var DEBUG = 1;
var INFO = 2;
var WARN = 3;
var ERROR = 4;
var NONE = 5;

var levels = ['VERBOSE ', 'DEBUG   ', 'INFO    ', 'WARN    ', 'ERROR   ', 'NONE    '];

// Change it to the desired log level and filter
var CURRENT_LEVEL = INFO;
var FILTER = [/* eg: INFO, ERROR */];

function log() {
  console.log.apply(console, arguments);
}

var Logger = {
  v: function v() {
    (FILTER.length === 0 || FILTER.indexOf(VERBOSE) >= 0)
    && VERBOSE >= CURRENT_LEVEL
    && log.apply(undefined, [levels[VERBOSE]].concat(Array.prototype.slice.call(arguments)));
  },

  d: function d() {
    (FILTER.length === 0 || FILTER.indexOf(DEBUG) >= 0)
    && DEBUG >= CURRENT_LEVEL
    && log.apply(undefined, [levels[DEBUG]].concat(Array.prototype.slice.call(arguments)));
  },

  i: function i() {
    (FILTER.length === 0 || FILTER.indexOf(INFO) >= 0)
    && INFO >= CURRENT_LEVEL
    && log.apply(undefined, [levels[INFO]].concat(Array.prototype.slice.call(arguments)));
  },

  w: function w() {
    (FILTER.length === 0 || FILTER.indexOf(WARN) >= 0)
    && WARN >= CURRENT_LEVEL
    && log.apply(undefined, [levels[WARN]].concat(Array.prototype.slice.call(arguments)));
  },

  e: function e() {
    (FILTER.length === 0 || FILTER.indexOf(NONE) >= 0)
    && NONE >= CURRENT_LEVEL
    && log.apply(undefined, [levels[NONE]].concat(Array.prototype.slice.call(arguments)));
  },

  setLoggerLevel(level) {
    level = isNaN(level) ? CURRENT_LEVEL : level;
    level = (level > 5) ? 5 : level;
    level = (level < 0) ? 0 : level;

    CURRENT_LEVEL = level;
  },

  setFilters(args) {
    FILTER = Array.isArray(args) ? args : [];
  }
};

module.exports = Logger;
