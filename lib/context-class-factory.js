'use strict';

const util = require('./util');
const Log = require('./simple-logger');
const TAG = 'context-class-factory';
const CLS = 'ContextClass';

/**
* Creates a new ContextClass function, its definition is declared inside this
* function so that each call produces a different ContextClass function.
*/
module.exports = function() {
  const _key = {};
  let identifier = 0;

  /**
  * Creates a new context to hold all the keywords from a specific database
  * @NOTE: You should NOT specify any argument.
  */
  function ContextClass (key) {
    let context = this;
    const isSubContext = !!key && key === _key;
    this.isRoot = !isSubContext;
    context.identifier = ++identifier;
    context.isContext = true;

    util.assert(!key || isSubContext, 'ContextClass constructor should not have arguments.');

    if(this.isRoot) {
      Log.v(CLS, 'constructor', 'New root context created.', 'identifier: ' + this.identifier);
      context = this._newBaseContext();
      this.context = context;
      // Once the root context is just a wrapper, the changes should be  made
      // on its 'context' property.
      this.getQuery = () => { return context.getQuery() };
      this.setQuery = (sql) => { return context.setQuery(sql) };
      this.appendQuery = (sql) => { return context.appendQuery(sql) };

    } else {
      this._stash = {};
    }

    context.sql = '';
    context.parent = this;

  };

  ContextClass.prototype._newBaseContext = function () {
    const context = this.newContext();
    context.isSubContext = false;
    return context;
  }

  ContextClass.prototype.newContext = function () {
    const root = this.getRootContext();
    const parent = this.getCurrentContext();
    const context = new ContextClass(_key);

    context.parent = parent;
    context.isSubContext = true;
    root.currentContext = context;
    Log.v(CLS, 'newContext', 'New sub context created.', 'identifier: ' + this.identifier);
    return context;
  }

  ContextClass.prototype.proxyContext = function (proxyCallback) {
    const root = this.getRootContext();
    const context = new ContextClass(_key);

    context.parent = root;
    context.isSubContext = true;
    context.isProxy = true;
    context.proxy = () => {
      proxyCallback && proxyCallback.apply(context.getCurrentContext())
    };
    Log.v(CLS, 'proxyContext', 'New proxy created.', 'identifier: ' + context.identifier);
    return context;
  }

  ContextClass.prototype.destroyContext = function () {
    const root = this.getRootContext();
    const current = this.getCurrentContext();
    const parent = current.parent;

    Log.v(CLS, 'destroyContext', 'Destroying context: '+ current.identifier +', returning the previous context: ' + parent.identifier);
    util.assert( current.isSubContext, 'You can\'t destroy the base context.' );

    root.currentContext = parent;
    return parent;
  }

  ContextClass.prototype.getRootContext = function() {
    let context = this;
    while (!context.isRoot) {
      context = context.parent;
    }
    return context;
  }

  ContextClass.prototype.isBaseContext = function() {
    const root = this.getRootContext();
    return this === root || this === root.context;
  }

  ContextClass.prototype.getCurrentContext = function() {
    return this.getRootContext().currentContext;
  }

  ContextClass.prototype.retrieve = function(key) {
    let stash = this.isRoot ? this.context._stash : this._stash;

    stash[key] = stash[key] || {isStash: true};
    return stash[key];
  }

  return ContextClass;
}
