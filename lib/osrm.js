'use strict';

const KF = require('./keyword-factory');
const CCF = require('./context-class-factory');
const util = require('./util');
const assert = util.assert;

const KEY = 'osrm';

const KEYWORD_PROPS = {
  callable: true,
  branch: false,
  merge: false
}

const OSRM = {};
let plugins;


/**
* Registers a new keyword group. Each group could have custom keywords or just
* map the keywords of a specific database.
*
* @param {array} pluginList - An array of plugin objects holding the following props:
* ### @property {string} name - The keyword group name. (eg: MySQL, Oracle, SQL)
* ### @property {object} config - The configuration properties applied to all keywords. (optional)
* ### @property {array} keywords - The keyword object list.
*/
OSRM.register = function(pluginList) {
  let name, config, factory, keywords;

  assertsPluginList (pluginList);
  plugins = pluginList;

  plugins.forEach((plugin) => {
    assertsPlugin (plugin);

    // CCF returns a new ContextClass function (Not an instance), so for each
    // plugin a new ContextClass with same properties (but diferent prototype)
    // will be created
    const ContextClass = CCF();

    config = plugin.config || {};
    config.keywordProps = Object.assign({}, config.keywordProps, KEYWORD_PROPS);

    factory = KF(config);
    keywords = plugin.keywords;

    // Assign each keyword to the ContextClass prototype
    keywords.forEach((keyword) => {
      _defineKeyword (
        ContextClass.prototype,
        factory.create(keyword),
        ContextClass
      );
    });

    // Exposes a function that instantiates a new ContextClass holding all
    // keywords of the current plugin
    OSRM[plugin.name] = () => {
      const rootContext = new ContextClass();
      return rootContext;
    }

    // Let the plugin expose other properties or functions.
    plugin.expose && plugin.expose( OSRM[plugin.name] );

  });
};

/**
* Assigns the keyword name to the target with a custom getter.
*
* @param {object} target - The target to assign the keyword to.
* @param {object} kw - The built keyword.
* @param {ContextClass} ContextClass - The current keyword group class definition
*/
function _defineKeyword(target, kw, ContextClass) {
  const definition = {
    configurable: false,
    enumerable: true
  };

  definition.get = function () {
    const isRoot = this.isRoot;
    let proxy = this.proxy;
    let context = this.getCurrentContext();
    let stash;

    // If this call was made on the root context and the current keyword
    // isn't a callable it is being assigned to a reference and, therefore,
    // shouldn't execute onCall directly, instead it return a proxied reference.
    // @NOTE This assumption was made because non-callable keywords never starts
    // a query, so it won't be invoked from the root context. If this assumption
    // becomes wrong in the future this code should be modified.
    if(isRoot && kw.callable === false) {
      return context.proxyContext(onCall);
    }

    if (kw.callable === false) {
      return onCall();
    }

    return onCall;

    // Functions //

    function onMerge (currContext, prevContext) {
      plugins.forEach((plugin) => {
        plugin.onMerge(currContext, prevContext, kw);
      });
      return currContext;
    }

    function onCall () {
      context = runProxy();

      if (kw.branch) {
        context = context.newContext();
        stash = context.retrieve(KEY);
        stash.onMerge = onMerge;
        return context;
      }

      if (kw.merge) {
        context = context.getCurrentContext();
        stash = context.retrieve(KEY);
        const currContext = context.destroyContext();
        stash.onMerge(currContext, context);
        return currContext;
      }

      plugins.forEach((plugin) => {
        plugin.onCall.call(null, kw, context, arguments);
      });

      return context;
    }

    function runProxy () {
      // the proxy could change the currentContext
      proxy && proxy();
      return context.getCurrentContext();
    }
  }

  Object.defineProperty(target, kw.identifier, definition);
}

module.exports = OSRM;

// *********************  ASSERTS  ************************* //

function assertsPluginList (plugins) {
  assert (
    util.isNonEmptyArray(plugins),
    'You must specify at least one plugin.'
  );
}

function assertsPlugin (plugin) {
  assert (
    util.isObject(plugin),
    'Parameter type mismatch. Expected "plugin" to be an object'
  );

  assert (
    util.isNonEmptyString (plugin.name),
    'Missing required "name" property'
  );

  assert (
    util.isNonEmptyArray (plugin.keywords),
    'The given plugin "' + plugin.name + '" has no keywords'
  );
}
