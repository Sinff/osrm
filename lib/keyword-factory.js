'use strict';

const util = require('./util');

let _id = 1;

function KeywordFactory (config) {
  config = config || {};
  assertsKeywordFactory(config);

  const factory = {};
  const keywordProps = config.keywordProps;

  function Keyword() {}
  Keyword.prototype.toString = function()  {
    return this.head + ' ' + this.tail;
  }

  Object.assign(Keyword.prototype, keywordProps);
  Keyword.prototype.keywords = {};

  // do a deep clone
  Keyword.prototype.clone = function () {
    const kw = new Keyword();
    Object.assign(kw, this);

    if(this.on) {
      kw.on = {};
      Object.keys(this.on).forEach((key) => {
        kw.on[key] = Object.assign({}, this.on[key]);
      });
    }
    return kw;
  }

  // returns a cloned keyword with the "operation" properties merged
  Keyword.prototype.resolve = function (key) {
    let props = this.on && this.on[key];

    const clone = this.clone();
    Object.assign(clone, props);
    return clone;
  }

  // Create a new keyword
  factory.create = function (rawKeyword) {
    const keyword = Keyword.prototype.keywords[rawKeyword.identifier] || new Keyword();
    mergeKeywordProps(rawKeyword, keyword);
    assertsKeyword(keyword);
    if(keyword.section) {
      keyword.section = _id++;
    }
    // exposes each new keyword to its prototype, so any keyword can be used to access others.
    Keyword.prototype.keywords[keyword.identifier] = keyword;
    return keyword;
  }

  return factory;
}

function mergeKeywordProps(source, target) {
  Object.keys(source).forEach((key) => {
    if(typeof source[key] === 'object') {
      target[key] = target[key] || {};
      mergeKeywordProps(source[key], target[key]);

    } else if (source[key] === undefined) {
      delete target[key];

    } else {
      target[key] = source[key];
    }
  });
}

module.exports = KeywordFactory;


// ******************** ASSERTS ********************** //
function assertsKeyword (keyword) {
  util.assert(
    util.isNonEmptyString(keyword.identifier),
    'The current keyword does not have a "identifier" property.'
  );

};

function assertsKeywordFactory (config) {
  util.assert(
    !util.isOk(config.keywordProps) || !util.isOk(config.keywordProps.identifier),
    'You can not specify a "identifier" property on the keywordProps object.'
  );
}
