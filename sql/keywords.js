'use strict';

/**
* List of currently supported keywords. Creating a new keyword is as simple as creating an object on this list.
*
* The supported properties are:
* @property {string} head - The opening part of keyword. eg: 'SELECT ('
* @property {string} tail - The closing part of keyword. eg: ')'
* @property {string} bodySeparator - The string used to separate each param passed to onCall (defaults to comma)
* @property {string} keySeparator - The string used to separate each keyword, the default is space.
* @property {boolean} callable - Indicates if the keyword is exposed like a function (true) or and object (false).
* @property {boolean} branch - Means that subsequent calls will happen in a separeted context until some keyword
* with 'merge' property is called. Branched keywords appends the 'tail' only after merge.
* @property {boolean} merge - Means that the current scope should be merged with the previous one.
* @property {boolean} schema - Means that the keyword should be used to build an entity.
* @property {number} min - The minimum number of parameters required by the keyword (conflicts with callable:false)
* @property {number} max - The maximum number of parameters allowed by the keyword (conflicts with callable:false)
* @property {boolean} multi - Specify if array parameters should be spread
* @property {boolean} alias - Specify if the alias should be exposed (adding "AS <alias>" on the query)
* @property {boolean} label - Specify if the column should be referenced by its alias name.
* @property {boolean} prefix - Specify if the table name should be prefixed on column name.
* @property {boolean} name - Specify if the column should be referenced by its name (opposite of label)
* @property {string} default - Specify a default value to be used when no parameter was given.
* @property {string} type - Specify the query type ('create', 'read', 'update', 'delete'). Only root keywords should specify this property.
* @property {object} on - an object to hold properties based on type.
* @property {boolean} visiblesOnly - Wheter should remove invisible fields
* @property {boolean} section - Specify if the keyword starts a new section, for example, SELECT and FROM starts a section,
* so every other non-section keyword called after SELECT and before FROM is contained in the SELECT section. There is no
* specific logic depending on it, the only purpose is to tell the handler implementors what was the last relevant keyword called.
* @property {boolean} fn - Specify if the keyword represents an SQL function, eg: MAX, COUNT, etc
*/
module.exports = {
  SELECT: {
    head: 'SELECT',
    type: 'read',
    alias: true,
    handler: 'select',
    extractFields: true,
    visiblesOnly: true,
    section: true
  },
  SELECT_DISTINCT: {
    head: 'SELECT DISTINCT',
    type: 'read',
    alias: true,
    handler: 'select',
    section: true
  },
  WHERE: {
    head: 'WHERE',
    callable: false,
    section: true
  },
  INSERT: {
    head: 'INSERT',
    type: 'create',
    callable: false,
    section: true
  },
  INTO: {
    head: 'INTO',
    prefix: false,
    min: 1,
    max: 1,
    handler: 'insert'
  },
  UPDATE: {
    head: 'UPDATE',
    type: 'update',
    prefix: false,
    min: 1,
    max: 1,
    handler: 'update',
    section: true
  },
  VALUES: {
    head: 'VALUES (',
    tail: ')',
    on: {
      create: { handler: 'valuesInsert' },
      update: { handler: 'valuesUpdate' }
    }
  },
  DELETE: {
    head: 'DELETE',
    type: 'delete',
    callable: false,
    section: true
  },
  SET: {
    head: 'SET',
    callable: false,
    section: true
  },
  FROM: {
    head: 'FROM',
    name: true,
    on: {
      delete: { alias: false, prefix: false }
    },
    label: false,
    alias: true,
    section: true
  },
  AND: {
    head: 'AND',
    callable: false
  },
  OR: {
    head: 'OR',
    callable: false
  },
  INNER: {
    head: 'INNER',
    callable: false
  },
  LEFT: {
    head: 'LEFT',
    callable: false
  },
  RIGHT: {
    head: 'RIGHT',
    callable: false
  },
  JOIN: {
    head: 'JOIN',
    reference: true,
    alias: true,
    name: true,
    label: false,
    min: 1,
    max: 2,
    handler: 'join',
    section: true
  },
  GROUP_BY: {
    head: 'GROUP BY',
    min: 1,
    section: true
  },
  ORDER_BY: {
    head: 'ORDER BY',
    min: 1,
    section: true
  },
  IN: {
    head: 'IN (',
    tail: ')',
    callable: true,
    min: 1,
    multi: true
  },
  ASC: {
    head: 'ASC',
    callable: false
  },
  DESC: {
    head: 'DESC',
    callable: false
  },
  HAVING: {
    head: 'HAVING',
    callable: false
  },
  LTE: {
    bodySeparator: '<='
  },
  LT: {
    bodySeparator: '<'
  },
  GTE: {
    bodySeparator: '>='
  },
  GT: {
    bodySeparator: '>'
  },
  EQ: {
    bodySeparator: '=',
    callable: false
  },
  EQUALS: {
    bodySeparator: '=',
    on: {
      read: { prefix: true }
    },
    prefix: false,
  },
  BETWEEN: {
    head: 'BETWEEN',
    bodySeparator: ' AND '
  },
  COUNT: {
    head: 'COUNT(',
    tail: ')',
    headTailSeparator: '',
    fn: true,
    on: {
      read: { postHandler: 'comma-on-select' }
    },
    prefix: true,
    label: false,
    name: true,
    default: '*',
    convey: true
  },
  MAX: {
    head: 'MAX(',
    tail: ')',
    headTailSeparator: '',
    fn: true,
    on: {
      read: { postHandler: 'comma-on-select' }
    },
    prefix: true,
    label: false,
    name: true
  },
  ON: {
    head: 'ON',
    callable: false
  },
  AS: {
    head: 'AS',
    min: 1,
    max: 1,
    aliasOnly: true,
    handler: 'as'
  },

 /* *********************************************************************** *
  *                           CUSTOM KEYWORDS                               *
  * *********************************************************************** */

  COLS: {
    head: '(',
    tail: ')',
    min: 1,
    on: {
      read: {
        handler: 'select',
        alias: true, extractFields: true, prefix: true
      }
    },
    prefix: false,
    handler: 'cols'
  },
  VALS: {
    head: '(',
    tail: ')',
    min: 1
  },
  ASSIGN: {
    bodySeparator: '=',
    min: 2,
    max: 2,
    prefix: false,
    handler: 'assign'
  },
  COL: {
    prefix: true,
    name: true,
    min: 1,
    max: 1,
    on: {
      read: { postHandler: 'comma-on-select' }
    }
  }
};
