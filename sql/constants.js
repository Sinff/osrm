'use strict';

module.exports = {
  COLS_TEMP_SCHEMA      : '_$cols',
  INSERT_SCHEMA         : '_$insSchm',
  ROOT_TYPE             : '_$rootTp',
  ORDERED_COLS          : '_$ordCols',
  SHOULD_FILTER         : '_$shdFlt',
  HAS_VALUES_GENERATED  : '_$hasGen',
  STEPS                 : '_$steps',
  SQL_PARSERS_STASH     : '_$sqlStash',
  CURRENT_STEPS         : '_$currStep',
  HISTORIC              : '_$historic',
  ORDERED_FIELDS_KEY    : '_$orderedFields',
  ASSIGN_CALLED         : '_$assign',
  UPDATE_SCHEMA         : '_$updSchm',
  SECTION_KEY           : '_$section',
  LAST_REQUEST          : '_$lastReq',
  AS_AND_ALIAS_KEY      : '_$asAlias'
};
