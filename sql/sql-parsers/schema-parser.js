'use strict';

function SchemaParser () {};

SchemaParser.prototype.canParseSql = function (kw, data, ctx) {
  return data.isSchema;
}

SchemaParser.prototype.parseSql = function (kw, val) {
  const name = kw.label ? val.alias : val.name;
  const alias = kw.alias ? kw.keySeparator + kw.keyAliasSeparator + kw.keySeparator + val.alias : '';
  return name + alias;
}

module.exports = SchemaParser;
