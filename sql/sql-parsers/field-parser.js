'use strict';

function FieldParser () {};

FieldParser.prototype.canParseSql = function(kw, data, stash) {
  return data.isField;
}

FieldParser.prototype.parseSql = function(kw, val, stash)  {

  const prefix = kw.prefix ? val.parent.alias + kw.keyPropAccessor : '';
  const name = kw.name ? val.field : (kw.label && val.alias) || '';
  const alias = kw.alias ? kw.keySeparator + kw.keyAliasSeparator + kw.keySeparator + val.alias : '';

  return prefix + name + alias;
}

module.exports = FieldParser;
