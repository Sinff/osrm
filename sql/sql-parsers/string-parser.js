'use strict';

function StringParser () {};

const charsRegex = /[\0\b\t\n\r\x1a\"\'\\]/g;
const charsMap = {
  '\0': '\\0',
  '\b': '\\b',
  '\t': '\\t',
  '\n': '\\n',
  '\r': '\\r',
  '\x1a': '\\Z',
  '"': '\\"',
  '\'': '\\\'',
  '\\': '\\\\'
};

function escapeString(val) {
  let chunkIndex = charsRegex.lastIndex = 0;
  let escaped = '';
  let match;

  while ((match = charsRegex.exec(val))) {
    escaped += val.slice(chunkIndex, match.index) + charsMap[match[0]];
    chunkIndex = charsRegex.lastIndex;
  }

  if (chunkIndex === 0) { return val; }
  return chunkIndex < val.length ? escaped + val.slice(chunkIndex) : escaped;
}

StringParser.prototype.canParseSql = function (kw, data, ctx) {
  return data.string;
};

StringParser.prototype.parseSql = function (kw, data, ctx) {  
  // if this keyword is 'branch' the val is a parsed SQL comming from the other context
  return !kw.branch ? "'" + escapeString(data.value) + "'" : data.value;
}

module.exports = StringParser;
