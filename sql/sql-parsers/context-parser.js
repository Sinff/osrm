'use strict';

function ContextParser () {};

ContextParser.prototype.canParseSql = function (kw, data) {
  return data.sql;
};

ContextParser.prototype.parseSql = function (kw, data)  {
  return data.value;
};

module.exports = ContextParser;
