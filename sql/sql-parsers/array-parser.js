'use strict';

function ArrayParser () {};

ArrayParser.prototype.canParseSql = function (kw, data) {
  return data.array;
};

ArrayParser.prototype.parseSql = (kw, data, ctx) => {
  const val = data.value;
  let response = '', i;

  for(i = 0; i < val.length - 1; i++) {
    response += val[i].value + kw.bodySeparator;
  }

  // put the last element on response without bodySeparator
  if(val.length > 0) {
    response += val[i].value;
  }

  return response;
}

module.exports = ArrayParser;
