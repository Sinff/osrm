'use strict';

function PrimitiveParser () {};

PrimitiveParser.prototype.canParseSql = function (kw, data, ctx) {
  return data.number || data.boolean || data.invalid || data.date;
}

PrimitiveParser.prototype.parseSql = function (kw, data, ctx) {
  const value = data.value instanceof Date ? '\'' + data.value.toUTCString() + '\'' : data.value;
  return data.invalid && ( !kw.default && 'NULL' || kw.default ) || value;
}

module.exports = PrimitiveParser;
