'use strict';

const util = require('../../lib/util');
const assert = util.assert;
const ALIAS_KEY = require('../constants').AS_AND_ALIAS_KEY;

function AliasParser () {};

AliasParser.prototype.canParseSql = function (kw, data, stash) {
  const map = stash.parent[ALIAS_KEY];
  return (data.isSchema || data.isField) &&
         (kw.aliasOnly || (map && map[data.alias]));
};

AliasParser.prototype.parseSql = (kw, val, ctx) => {
  return val.alias;
}

module.exports = AliasParser;
