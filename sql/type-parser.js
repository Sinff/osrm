'use strict';

const util = require('../lib/util');

const TYPE_RESOLVER_PARSER = {
  canParseSql: (kw, val, ctx) => {
    let TYPE;

    if( Array.isArray(val) ) {
      TYPE = 'array';
    } else if( util.isOk(val) ) {
      TYPE = (typeof val);
    } else {
      TYPE = 'invalid';
    }

    // add to ctx a property telling the current type
    ctx.type = {};
    ctx.type[TYPE] = true;

    return false;
  }
}

module.exports = TYPE_RESOLVER_PARSER;
