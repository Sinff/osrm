'use strict';

let util = require('../../lib/util');
let assert = util.assert;

const PARENT_KEY = '__parentSchema';
const DEFAULT_STASH_KEY = 'schemas';
const REQUEST_STASH_KEY = 'request';
const FILTER_SCHEMA = 'filterSchema';
const SHOULD_FILTER = 'shouldFilter';
const ORDERED_FIELDS_KEY = 'orderedFields';
const COLS_TEMP_SCHEMA = 'tempSchema';
const HAS_VALUES_GENERATED = 'generatedFields';
const CONVEY_KEYWORD = 'convey';
const PREVIOUS_KEYWORD = 'previous';

function SchemaHandler (schemaFactory) {
  this.Schema = schemaFactory;
}

SchemaHandler.prototype.hasAnySchema = function (ctx) {
  return !!ctx.schemas;
}

SchemaHandler.prototype.canHandle = function (val) {
  return val && (val.isField || val.isSchema || val instanceof FieldList);
}

/**
* @param {object} ctx - The target to assign the schemas.
* @param {array} fields - Array of fields or schemas (accepts arguments)
* @param {string} [stashKey] - A key to map the schemas info
* @return {array} - Return a list of rejected fields or an empty array
*/
SchemaHandler.prototype.addFieldList = function (ctx, fields, stashKey) {
  let arr, added, rejected = [];
  stashKey = stashKey || DEFAULT_STASH_KEY;

  fields.forEach((fld) => {
    added = this.addField(ctx, fld, stashKey);
    !added && (rejected.push(fld));
  });

  return rejected;
}

SchemaHandler.prototype.extractSchemaFields = function(schema) {
  if(schema && schema.isSchema) {
    return schema.fields;
  } else if (schema instanceof FieldList) {
    return schema;
  }

  return undefined;
}

/**
* @param {object} ctx - A place to store the schemas.
* @param {array} field - The field to be added (or schema) on ctx schemas
* @param {string} [stashKey] - A key to map the schemas info
* @return {boolean} - true if the fields was added, false otherwise
*/
SchemaHandler.prototype.addField = function(ctx, field, stashKey) {
  if(!this.canHandle(field)) {
    return false;
  }
  stashKey = stashKey || DEFAULT_STASH_KEY;

  const schemaFields = this.extractSchemaFields(field);
  if(schemaFields) {
    const arr = util.propertiesAsArray(schemaFields);
    return this.addFieldList(ctx, arr);
  }

  let sch = field.parent;

  this._buildSchema(ctx, sch, stashKey);

  ctx[stashKey][sch.alias].addField(field);
  return true;
}

SchemaHandler.prototype.getSchemas = function(ctx, stashKey) {
  stashKey = stashKey || DEFAULT_STASH_KEY;
  return ctx[stashKey];
}

SchemaHandler.prototype._buildSchema = function (ctx, sch, stashKey) {
  const Schema = this.Schema;
  stashKey = stashKey || DEFAULT_STASH_KEY;
  ctx[stashKey] = ctx[stashKey] || {};
  ctx[stashKey][sch.alias] = ctx[stashKey][sch.alias] || new Schema(sch.name, sch.alias, {});
}

SchemaHandler.prototype.addFieldRefs = function (ctx, schema, field) {
  if(!field) {
    return;
  }

  const sch = this.getSchema(schema);

  assert(sch, 'Parameter Type mismatch. Expected schema to be a "Schema", but found: ' + (typeof schema));
  assert(field.isField, 'Parameter Type mismatch. Expected field to be a "Field", but found: ' + (typeof field));

  this._buildSchema(ctx, sch);
  field = field.ref(ctx.schemas[sch.alias]);

  // Mark the reference schema as nested.
  field.reference.isNested = true;
  this.addField(ctx, field);
}

SchemaHandler.prototype.getField = function (ctx, field) {
  const config = field.parent;
  const schema = ctx.schemas[config.alias];
  return schema && schema.fields[field.property];
}

/**
* Retrieve all fields that aren't refs to other schema
* @param {object} ctx - A stash object
* @param {string} stashKey - The key used to retrieve the schemas
*/
SchemaHandler.prototype.getOwnFields = function(ctx, stashKey) {
  stashKey = stashKey || DEFAULT_STASH_KEY;
  if(!ctx[stashKey]) {
    return [];
  }
  const stash = ctx[stashKey];

  let fields = [];
  Object.keys(stash).forEach((key) => {
    fields = fields.concat(stash[key].keyFields).concat(stash[key].ownFields);
  });

  return fields;
}

/**
* Extract the schema from the given ctx
* @param {Field|Schema|FieldList} ctx - The object that holds the schema instance
* @return {Schema} - returns the extracted Schema.
*/
SchemaHandler.prototype.getSchema = function(ctx) {
  if(ctx instanceof FieldList) {
    return ctx[PARENT_KEY];
  }
  else if (ctx.isSchema) {
    return ctx;
  }
  else if (ctx.isField) {
    return ctx.parent;
  }
  return null;
}

/**
* Exposes the schema fields in a new FieldList
* @param {Schema} schema - The schema instance that holds the fields
* @return {FieldList} - The FieldList object with all extracted Field instances
*/
SchemaHandler.prototype.exposeFields = function(schema) {
  return new FieldList(schema);
}

SchemaHandler.prototype.isSchema = function(schema) {
  return !!schema && (schema.isSchema || schema instanceof FieldList);
}

/**
* This class is used to expose the fields from a given schema, it adds a non-enumerable property that points
* to the real schema.
*/
function FieldList (schema) {
  Object.assign(this, schema.fields);

  Object.defineProperty(this, PARENT_KEY, {
    enumerable: false,
    configurable: true,
    value: schema
  });
}

SchemaHandler.prototype.resolve = function (ctx, rows, stashKey) {
  stashKey = stashKey || DEFAULT_STASH_KEY;
  const schemas = ctx[stashKey];
  let curr, alias, output = {}, meta = {}, childMeta;
  let _schemas = [];
  // put each schema into "_schemas" array to avoid calling object.keys() inside the next for statement.
  Object.keys(schemas).forEach((key) => {
    curr = schemas[key];
    if (!curr.isNested) {
      _schemas.push(curr);
      output[key] = [];
    }
  });

  for(let i = 0; i < rows.length; i++) {
    for(let j = 0; j < _schemas.length; j++) {
      alias = _schemas[j].alias;
      meta[alias] = meta[alias] || {};
      childMeta = _schemas[j].resolve(rows[i], meta[alias], output, alias);
       
      if (!childMeta.hasAnyProp) {
        delete meta[alias];
        output[alias] = [];
      }
    }
  }

  return output;
}


module.exports = SchemaHandler;
