'use strict';

let util = require('../../lib/util');
let assert = util.assert;

function SqlHandler() {}

SqlHandler.prototype.initializeSql = function (context) {
  context.sql = context.sql || '';
}

SqlHandler.prototype.getSql = function (context) {
  this.initializeSql(context);
  return context.sql;
}

SqlHandler.prototype.appendSql = function (context, keyword, sql) {
  let keySeparator = keyword.keySeparator;

  this.initializeSql(context);
  keySeparator = (context.sql === '') ? '' : keySeparator;
  sql = keySeparator + sql;
  sql = keyword.trimHead ? sql.trimLeft() : sql;
  context.sql += sql;
}

module.exports = SqlHandler;
