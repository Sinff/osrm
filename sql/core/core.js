'use strict';

const util = require('../../lib/util');
const CONSTS = require('../constants');
const assert = util.assert;

const SQL_PARSERS_STASH = CONSTS.SQL_PARSERS_STASH;
const STEPS = CONSTS.STEPS;
const CURRENT_STEPS = CONSTS.CURRENT_STEPS;
const HISTORIC = CONSTS.HISTORIC;
const ROOT_TYPE = CONSTS.ROOT_TYPE;
const SECTION_KEY = CONSTS.SECTION_KEY;
const LAST_REQUEST = CONSTS.LAST_REQUEST;

function Core (stashKey) {
  this.stashKey = stashKey;
  this.parseValToSql = this.parseValToSql.bind(this);
  this.parseKwToSql = this.parseKwToSql.bind(this);
}

Core.prototype.setSqlParsers = function (parsers) {
  parsers.forEach((parser) => {
    assert (util.isFunction(parser.canParseSql) && util.isFunction(parser.parseSql), 'Invalid sql parser, it must specify the functions canParseSql and parseSql');
  });
  this.sqlParsers = parsers;
};

Core.prototype.setKeywordHandlers = function (handlers) {
  const handlersMap = {};
  handlers.forEach((curr) => {
    assert (util.isObject(curr) && util.isNonEmptyString(curr.name) && util.isFunction(curr.handler), 'Invalid keyword handler, it must specify a "name" property and a "handler" function. ');
    handlersMap[curr.name] = curr.handler;
    curr.handler.id = curr.name;
    curr.handler.isPostHandler = curr.isPostHandler;
  });
  this.kwHandlers = handlersMap;
};

Core.prototype.setSchemaContextHandler = function (handler) {
  this.schCtxHandler = handler;
};

Core.prototype.setSqlContextHandler = function (handler) {
  this.sqlCtxHandler = handler;
};

/**
* Method called evey time a keyword is used.
* @param {Keyword} kw - The keyword being used
* @param {ContextClass} context - The current query execution context
* @param {array} args - The arguments object given to the keyword (when executed as a function, eg. SELECT(...))
*/
Core.prototype.onCall = function (kw, ctx, args) {
  const kwHand = this.kwHandlers;
  const stash = ctx.retrieve(this.stashKey);

  stash[HISTORIC] = stash[HISTORIC] || [];
  stash[HISTORIC] = stash[HISTORIC].concat({kw: kw, args: args});
  stash[SECTION_KEY] = kw.section || stash[SECTION_KEY];

  if(kw.type) {
    stash[ROOT_TYPE] = kw.type;
  }

  kw = kw.resolve(stash[ROOT_TYPE]);

  assert(isNaN(kw.min) || args.length >= kw.min, 'The keyword "' + kw.toString() + '" requires at least '+ kw.min +' arguments.');
  assert(isNaN(kw.max) || args.length <= kw.max, 'The keyword "' + kw.toString() + '" allows at most '+ kw.max +' arguments.');

  args = this.resolveArgs(args, kw);

  this.resolveSteps(args, kw, stash, kwHand);
}

Core.prototype.resolveSteps = function(args, kw, stash, kwHand) {
  let steps = (typeof kw.handler === 'string')
    ? kwHand[kw.handler](kw, args, stash)
    : kw.handler && kw.handler(kw, args, stash);


  if(!util.isOk(steps)) {
    steps = [{kw: kw, args: args}];
  }

  steps = util.isObject(steps) ? [steps] : steps;
  assert(Array.isArray(steps), 'Invalid keyword handler return type. Expected undefined, object or array of objects, but found: ' + typeof steps);

  // Runs the postHandler declared in the keyword (if there is one)
  const postHandler = (typeof kw.postHandler === 'string') ? kwHand[kw.postHandler] : kw.postHandler;
  if(postHandler) {
    assert(postHandler.isPostHandler, 'The handler "' + postHandler.id + '" was configured as "postHandler". postHandlers must have the property "isPostHandler" set to true.');
    steps = postHandler(steps, stash);
  }

  // Save the current request information for so the next calls could refer to it
  stash[LAST_REQUEST] = { kw: kw, argsCount: args.length };

  for(let i = 0; i < steps.length; i++) {
    let step = steps[i];

    if(step.skip) {
      continue;
    }

    step.kw = !step.kw ? kw : step.kw;

    // The 'resolve' flag means that the step have changed the keyword so it need to be resolved again
    if(step.resolve) {
      this.resolveSteps (step.args, step.kw, stash, kwHand);
      continue;
    }

    step.args = !step.args || Array.isArray(step.args) ? step.args : [step.args];
    step.refs = !step.refs || Array.isArray(step.refs) ? step.refs : [step.refs];

    assert(util.isObject(step), 'Invalid step type. Expected an object but found ' + typeof step);
    step.schema && this.resolveSchema(step, stash);
    !step.skipSql && this.resolveSql(step, stash);
  }
}

/**
* Method called when the context is about to merge. {@see osrm}
* @param {ContextClass} currContext - The parent context to receive the merged data
* @param {ContextClass} oldContext - The context being merged (destroyed)
* @param {Keyword} kw - The current keyword
*/
Core.prototype.onMerge = function (currContext, oldContext, kw) {
  let ostash = oldContext.retrieve(this.stashKey);
  this.sqlCtxHandler.initializeSql(ostash);

  let query = this.sqlCtxHandler.resolveKeyword(kw, [ostash]);
  let cstash = currContext.retrieve(this.stashKey);
  this.sqlCtxHandler.appendSql(cstash, kw, query);
}

/**
* @param {array} args - An array of arguments. Each element is resolved based on its types:
* ### Primitve, undefined or null are wraped by another object and its type is exposed.
* ### Objects and each of its props are wraped by another object and its type is exposed.
* ### Schema is returned as is. (The schema is extracted if there is one nested).
* ### Field is returned as is.
* ### Arrays have each of its elements wraped and appended to the response array.
* @param {Keyword} kw -The current keyword
* @param {number} depth - The max nesting to resolve args.
*/
Core.prototype.resolveArgs = function (args, kw, depth) {
  let resolvedArgs = [];
  let subArgs = [];
  let data, val;
  depth = isNaN(depth) ? kw.depth : depth ;

  if(depth <= 0) {
    return args;
  }

  for(let i = 0; i < args.length; i++) {
    val = args[i];
    data = {};
    if (val === undefined) {
      data.invalid = true;
      data.type = 'undefined';

    } else if(val === null) {
      data.null = true;
      data.type = 'null';

    } else if (Array.isArray(val)) {
      let tempArgsArr = [];
      for(let i = 0; i < val.length; i++) {
        tempArgsArr = tempArgsArr.concat(this.resolveArgs( [val[i]], kw, depth-1 ));
      }
      if(kw.extractElements) {
        subArgs = subArgs.concat(tempArgsArr);
        continue;
      }
      data.type = 'array';
      data.array = true;
      val = tempArgsArr;

    } else if (this.schCtxHandler.isSchema(val)) {
      val = this.schCtxHandler.getSchema(val);

      if(kw.extractFields) {
        resolvedArgs = resolvedArgs.concat(this.resolveArgs(val.keyFields.concat(val.ownFields), kw, depth-1));
        continue;
      }

      resolvedArgs.push(val);
      continue;

    } else if (val.isField && val.parent && val.parent.isSchema) {
      if(kw.visiblesOnly === true && !val.isVisible) {
        continue;
      }

      resolvedArgs.push(val);
      continue;

    } else if (val.isStash || val.isContext) {
      data.stash = true;
      val = val.isContext ? val.retrieve(this.stashKey) : val;
      data.sql = this.sqlCtxHandler.getSql(val);
      data.schemas = this.schCtxHandler.getSchemas(val);
      data.steps = val[HISTORIC];
      data.type = 'query';

    } else if (val instanceof Date) {
      data.type = 'date';
      data.date = true;

    } else if (util.isObject(val)) {
      let tempArg, tempObj = {};
      Object.keys(val).forEach((key) => {
        tempArg = this.resolveArgs([val[key]], kw, depth-1);
        tempArg = tempArg.length === 1 ? tempArg[0] : tempArg;
        tempObj[key] = tempArg;
      });
      val = tempObj;
      data.object = true;
      data.type = 'object';

    } else {
      data[(typeof val)] = true;
      data.type = typeof val;

    }

    data.value = val;
    data.isWrapper = true;
    resolvedArgs.push(data);
    //args[i] && args[i].isContext ? args[i].retrieve(this.stashKey) : args[i];
  }

  return resolvedArgs.concat(subArgs);
}

Core.prototype.resolveSchema = function(step, stash) {
  const args = step.args;

  if(step.reference) {
    const ref = step.refs && step.refs[0];
    this.schCtxHandler.addFieldRefs(stash, args[0], ref);

  } else if (args) {
    this.schCtxHandler.addFieldList(stash, args);
  }
}

Core.prototype.resolveSql = function (step, stash) {
  const sqlStash = stash[SQL_PARSERS_STASH] = stash[SQL_PARSERS_STASH] || {};
  sqlStash.parent = stash;
  sqlStash.rootKeyword = stash.rootKeyword;
  let query = this.parseKwToSql(step.kw, step.args, sqlStash);
  this.sqlCtxHandler.appendSql(stash, step.kw, query);
}

Core.prototype.parseValToSql = function (kw, data, sqlStash) {
  const parsers = this.sqlParsers;

  for(let i = 0; i < parsers.length; i++) {
    if (parsers[i].canParseSql(kw, data, sqlStash)) {
      return parsers[i].parseSql(kw, data, sqlStash, this.parseValToSql);
      break;
    }
  }
}

Core.prototype.parseKwToSql = function (kw, args, kwStash) {
  const head = (kw.head || '').trim();
  const tail = (kw.tail || '').trim();
  var i = 0, count = 0;

  let str = (head !== '') ? head + kw.headTailSeparator : '';
  str = kw.trimBody ? str.trimRight() : str;
  let value;
  for (i = 0; i < args.length; i++) {
    value = this.parseValToSql(kw, args[i], kwStash);
    if(util.isOk(value)) {
      str += (count > 0) ? kw.bodySeparator + value : value;
      count++;
    }
  }
  str += (tail !== '') ? kw.headTailSeparator + tail : '';
  str = str.trim();

  return str;
};

Core.prototype.getCurrentSteps = function(ctx) {
  return ctx.retrieve(this.stashKey)[CURRENT_STEPS];
}

module.exports = Core;
