'use strict';

let util = require('../../lib/util');
let assert = util.assert;

const Field = require('./schema-field');

let identifier = 0;
let customFieldProps = [];

function Schema (source, alias, fields) {
  schemaConstructorAsserts (source, alias, fields);

  this.fields = {};
  this.keyFields = [];
  this.refFields = [];
  this.ownFields = [];
  this.name = source;
  this.alias = alias || source;
  this.size = 0;
  this.id = identifier++;
  this.isSchema = true;

  let fld;
  Object.keys(fields).forEach((key) => {
    fld = new Field(this, key, fields[key]);
    this.fields[key] = fld;

    if (fld.isKey) {
      this.keyFields.push(fld);
    } else if(fld.isRef) {
      this.refFields.push(fld);
    } else {
      this.ownFields.push(fld);
    }
    this.size++;
  });
}

Schema.prototype.toString = function () {
  return '{ id: ' + this.id
    + ', alias: ' + this.alias
    + ', name: ' + this.name
    + ', size: ' + this.size
    + ', keys: ' + this.keyFields.length
    + ', refs: ' + this.refFields.length
    + ', owns: ' + this.ownFields.length
    + ' }';
}

Schema.prototype.extractKey = function (row) {
  const fields = this.keyFields;
  let key = '';
  let i;

  assert ( fields.length > 0, 'The Schema "'+ this.name +'" has no "key" property defined. You must specify at least one "key" property per object' );

  for(i = 0; i < fields.length; i++) {
    key += row[fields[i].alias] || '';
  }

  return key;
}

Schema.prototype.resolve = function(row, meta, output, name) {
  const key = this.extractKey(row) + name;
  let hasSomeValue = false, hasRefValue = false;
  let val, i, fld, currOutput;
  let childMeta;

  if(!meta[key]) {
    meta[key] = {
      output: {},
      meta: {}
    };

    for(i = 0; i < this.ownFields.length; i++) {
      fld = this.ownFields[i];
      fld.assignValue(meta[key].output, row[fld.alias])
      && (meta[key].hasAnyProp = true);
    }

    for(i = 0; i < this.keyFields.length; i++) {
      fld = this.keyFields[i];
      fld.assignValue(meta[key].output, row[fld.alias])
      && (meta[key].hasAnyProp = true);
    }
  }

  for(i = 0; i < this.refFields.length; i++) {
    fld = this.refFields[i];

    // if the field hasn't pointed the reference yet it should be ignored
    if(fld.reference) {
      meta[key].output[fld.property] = meta[key].output[fld.property] || (fld.isMany && [] );
      meta[key].meta = meta[key].meta || {};
      childMeta = fld.reference.resolve(row, meta[key].meta, meta[key].output, fld.property);
      meta[key].hasAnyProp = childMeta.hasAnyProp || meta[key].hasAnyProp;

      if (!childMeta.hasAnyProp) {
        delete meta[key].output[fld.property];
        delete meta[key].meta[fld.property];
      }
    }
  }

  if(meta[key].hasAnyProp && !meta[key].isInOutput) {
    if(Array.isArray(output[name])) {
      output[name].push(meta[key].output);
    } else {
      output[name] = meta[key].output;
    }
    meta[key].isInOutput = true;
  }

  return meta[key];
}

Schema.prototype.addField = function(field) {
  let curr = this.fields[field.property];

  if(curr) {
    this.removeField(curr);
  }

  curr = new Field(this, field.property, field);

  if(curr.isRef) {
    this.refFields.push(curr);
  }
  else if(curr.isKey) {
    this.keyFields.push(curr);
  }
  else if(curr.isOwn) {
    this.ownFields.push(curr);
  }

  this.fields[curr.property] = curr;
  this.size++;
}

Schema.prototype.removeField = function (field) {
  let curr = this.fields[field.property];

  if (curr) {
    let idx;
    if(curr.isRef) {
      idx = this.refFields.indexOf(curr);
      this.refFields.splice(idx, 1);
    }
    else if(curr.isKey) {
      idx = this.keyFields.indexOf(curr);
      this.keyFields.splice(idx, 1);
    }
    else if(curr.isOwn) {
      idx = this.ownFields.indexOf(curr);
      this.ownFields.splice(idx, 1);
    }
    delete this.fields[curr.property];
    this.size--;
  }
}

function schemaConstructorAsserts (source, alias, fields) {
  const SOURCE = typeof source;
  const ALIAS = typeof alias;
  const FIELDS = typeof alias;

  assert ( util.isNonEmptyString(source), 'Parameter type mismatch. ' + ( SOURCE === 'string' ? '"source" can not be an empty string.' : 'Expected "source" to be a string but found ' + SOURCE ) );
  assert ( !util.isOk(alias) || util.isNonEmptyString(alias), 'Parameter type mismatch. ' + ( ALIAS === 'string' ? '"alias" can not be an empty string.' : 'Expected "alias" to be a string but found ' + ALIAS ) );
  assert ( util.isObject(fields), 'Parameter type mismatch. Expected "fields" to be an object but found ' + FIELDS );
}

function FACTORY (source, alias, fields, cloned) {
  let _source, _alias, _fields;

  // Case it was called with 'fields' as the first argument
  if(util.isObject(source)) {
    _fields = Object.assign({}, source);
    _source = source.SOURCE;
    _alias = source.ALIAS;
    delete _fields.SOURCE;
    delete _fields.ALIAS;

    return FACTORY(_source, _alias, _fields, true);
  }
  // Case it was called with 'fields' as the second argument
  else if(util.isObject(alias)) {
    _fields = Object.assign({}, alias);
    _source = source;
    _alias = alias.ALIAS;
    delete _fields.ALIAS

    return FACTORY(_source, _alias, _fields, true);
  }
  // Case all arguments was given
  else {
    fields = cloned ? fields : Object.assign({}, fields)
    return new Schema(source, alias, fields);
  }
}

FACTORY._configure = function (config) {
  config = config || {};

  const customFieldProps = Array.isArray(config.customFieldProps) ? config.customFieldProps : [];
  const parsers = Array.isArray(config.objParsers) ? config.objParsers : [];
  Field._configure(customFieldProps, parsers);
};

module.exports = FACTORY;
