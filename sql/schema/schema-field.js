'use strict';

let util = require('../../lib/util');
let assert = util.assert;

// TODO: The following properties will be shared through all Fields, it will prevent two different plugins to
// register separated customFieldProps and parser, so the whole Field class should be exposed by a wrapper
// function to create a closure around these variables and a new reference to Field class.
let _customFieldProps = [];
let _parsers = {};

/**
* @param {Schema} parent - The parent schema instance
* @param {string} property - The property name exposed by the schema.
* @param {(object|string|false)} [field] - The base field value. When it is a string the value will be used as
* the field name, when it resolves to false the field won't map to database and could be used as a placeholder
* to a reference, when it is an object the following properties are allowed:
* ### @property {string|false} [field] - The database column name. Any false value turns it in a virtual ref.
* ### @property {boolean} [isKey] - Specify if this field should be used as a unique identifier.
* ### @property {Schema} [mapTo] - The schema referenced by this field.
* ### @property {boolean} [isMany] - Specify if this field has a many relationship with mapTo.
* ### @property {boolean} [isDummy] - Specify if this field doesn't maps to a database column
* ### @property {boolean} [isVisible] - Specify if this field should be selected
* ### @property {function|string} [parser] - Specify a parser to handle the data retrieved from db
*/
function Field (parent, property, field) {
  fieldConstructorAsserts (parent, property, field);
  field = util.isString(field) ? {field: field} : field || {};

  this.property = property;
  this.parent = parent;
  this.field = field.field;

  if(field instanceof Field) {
    // if the parent alias has changed, the field should modify its alias too
    if(field.alias && parent.alias !== field.parent.alias) {
      this.alias = field.alias.replace(field.parent.alias, parent.alias);
    } else {
      this.alias = field.alias;
    }
  }

  if(!this.alias ) {
    this.alias = ( this.field )
      ? parent.alias + '_' + (field.alias || this.field)
      : undefined;
  }

  this.reference = field.mapTo || field.reference;

  this.isRef = !!this.reference || !this.field;
  this.isMany = field.isMany || false;
  this.isKey = field.isKey || false;
  this.isDummy = field.isDummy;
  this.isOwn = !this.isKey && !this.isRef && !this.isMany;
  this.isVisible = field.isVisible === false ? false : true;

  // 'parse' is the property used internally by the field (TODO: remove the parse)
  const parse = field.parser || field.parse;
  if(parse) {
    this.parse = typeof parse === 'string' ? _parsers[parse] : parse;
  }

  assertsField(this);

  if(_customFieldProps.length > 0) {
    this.customProps = {};
  }

  let hasCustom = false;
  _customFieldProps.forEach((prop) => {
    if(field[prop]) {
      this[prop] = field[prop];
      hasCustom = true;
      this.customProps[prop] = field[prop];
    }
  });
  this.raw = field.raw !== undefined ? field.raw : !hasCustom;

}

Field.prototype.toString = function() {
  return '{ parent: '
    + this.parent.alias + ', field: '
    + this.field + ', alias: '
    + this.alias + ', isRef: '
    + this.isRef + ', isMany: '
    + this.isMany + ', isOwn: '
    + this.isOwn + ', isKey: '
    + this.isKey + ' }';
}

function _setAsDummy (fld) {
  _setAsOwn(fld);
  fld.isDummy = true;
}

function _setAsOwn (fld) {
  fld.isRef = false;
  fld.isOwn = true;
  fld.isKey = false;
  fld.isMany = false;
}
function _setAsRef (fld) {
  fld.isRef = true;
  fld.isOwn = false;
  fld.isKey = false;
  fld.isDummy = false;
}
function _setAsKey (fld) {
  fld.isRef = false;
  fld.isOwn = false;
  fld.isKey = true;
}

Field.prototype.clone = function() {
  return new Field(this.parent, this.property, this);
}

/**
* Check only props relative to field (not parent).
*
* @param {Field} val - The other field to be compared
* @return {boolean} -
*/
Field.prototype.hasSameProps = function(val) {
  return val.isField
    && val.isKey === this.isKey
    && val.isOwn === this.isOwn
    && val.isRef === this.isRef
    && val.isMany === this.isMany
    && val.field === this.field
    && val.property === this.property;
}

Field.prototype.isField = true;

/**
* Exposes a 'key' property.
* @return {Field} - A modified version of the current field (with isKey set to true)
*/
Object.defineProperty(Field.prototype, 'key', {
  enumerable: true,
  configurable: false,
  get: function () {
    assert ( !this.isRef, 'It\'s not possible to set "' + this.property + '" as a key, it is a reference.' );

    if(this.isKey) {
      return this;
    }

    const clone = this.clone();
    _setAsKey(clone);
    return clone;
  }
});

/**
* Exposes a 'many' property.
* @return {Field} - A modified version of the current field (with isMany set to true)
*/
Object.defineProperty(Field.prototype, 'many', {
  enumerable: true,
  configurable: false,
  get: function () {

    if(this.isMany) {
      return this;
    }

    const clone = this.clone();
    _setAsRef(clone);
    clone.isMany = true;
    // references are virtual fields and doesn't have 'field' neither 'alias' property
    delete clone.field;
    delete clone.alias;
    return clone;
  }
});

/**
* Exposes a 'one' property.
* @return {Field} - A modified version of the current field (with isMany set to false)
*/
Object.defineProperty(Field.prototype, 'one', {
  enumerable: true,
  configurable: false,
  get: function () {

    if(!this.isMany) {
      return this;
    }

    const clone = this.clone();
    _setAsRef(clone);
    clone.isMany = false;
    // references are virtual fields and doesn't have 'field' neither 'alias' property
    delete clone.field;
    delete clone.alias;
    return clone;
  }
});

Object.defineProperty(Field.prototype, 'dummy', {
  enumerable: true,
  configurable: false,
  get: function () {

    assert(util.isNonEmptyString(this.alias), 'Dummy fields must have an alias.');

    const clone = this.clone();
    _setAsDummy(clone);

    // dummy fields doesn't
    delete clone.field;

    return clone;
  }
});

Object.defineProperty(Field.prototype, 'show', {
  enumerable: true,
  configurable: false,
  get: function () {

    if(this.isVisible) {
      return this;
    }
    const clone = this.clone();
    clone.isVisible = true;
    return clone;
  }
});

/**
* Creates a clone of the current field and assigns to it the given schema reference.
*
* @param {Schema} schema - A Schema instance to be referenced.
* @return {Field} - A modified clone of the current field.
* @throws {Error} - If the schema parameter isn't a Schema
*/
Field.prototype.ref = function(schema) {
  assert(schema && schema.isSchema, 'Parameter type mismatch. Expected "schema" to be a Schema, but found ' + (typeof schema));

  const clone = this.clone();
  _setAsRef(clone);
  clone.reference = schema;

  // references are virtual fields and doesn't have 'field' neither 'alias' property
  delete clone.field;
  delete clone.alias;
  return clone;
}


Field.prototype.assignValue = function (context, val) {
  const prop = this.property;

  if(!util.isOk(val) || !this.isVisible) {
    return;
  }

  if(!this.isMany) {
    if(!this.isRef && this.parse) {
      context[prop] = this.parse(val);

    } else {
      context[prop] = val;
    }

    return true;

  } else if(this.isMany) {
    context[prop] = context[prop] || [];
    context[prop].push(val);
    return true;

  } else {
    throw Error('Malformed context schema, missing required property.');
  }
}

function fieldConstructorAsserts (parent, property, field) {
  const parse = field && (field.parser || field.parse);
  const PARENT = typeof parent;
  const PROPERTY = typeof property;
  const COLUMN = typeof field;
  const PARSE = typeof parse;

  assert ( util.isOk(parent) && parent.isSchema, 'Parameter type mismatch. Expected "parent" to be a Schema but found ' + (PARENT) );
  assert ( util.isNonEmptyString(property), 'Parameter type mismatch. ' + ( PROPERTY === 'string' ? '"property" can not be an empty string.' : 'Expected "property" to be a string but found ' + PROPERTY  ) );
  assert ( !util.isOk(field) || util.isString(field) || util.isObject(field), 'Parameter type mismatch. Expected "field" to be a string, an object or null but found ' + COLUMN );
  assert ( !util.isOk(parse) || (PARSE === 'string' || PARSE === 'function'), 'Property type mismatch. Expected the parser to be a string, a function or to not be sepecified, but found: ' + PARSE );
}

function assertsField (field) {
  assert ( !field.isRef  || (field.isRef  && !field.isDummy && !field.isKey), 'Invalid state. A "ref"  field can not be set as "dummy" or "key".' );
  assert ( !field.isMany || (field.isMany && !field.isDummy && !field.isKey), 'Invalid state. A "many" field can not be set as "dummy" or "key".' );
}

function _defineCustomParser (parser) {
  Object.defineProperty(Field.prototype, parser.name, {
    enumerable: true,
    configurable: true,
    get: function () {
      const clone = this.clone();
      clone.parse = parser.parseObj;
      return clone;
    }
  });
}

Field._configure = function (customFieldProps, parsers) {
  _customFieldProps = customFieldProps;
  _parsers = {};

  // Exposes each custom parser by its name to all fields
  parsers.forEach((parser) => {
    if (parser.name && parser.parseObj) {
      _parsers[parser.name] = parser.parseObj;
      _defineCustomParser(parser);
    }
  });
};

module.exports = Field;
