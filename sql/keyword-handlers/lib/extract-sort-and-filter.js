'use strict';
/*
module.exports = function filterFields(ctx, values) {
  const schema = ctx[FILTER_SCHEMA];
  const isStashed = !!ctx[ORDERED_FIELDS_KEY];
  let fields = ctx[ORDERED_FIELDS_KEY];

  values = values[0].object ? values[0] : values;

  if(Array.isArray(values) && !fields) {
    throw new Error('You must specify the COLS keyword before the VALUE');
  }

  if(values.object) {
    // unwrap the values object
    values = values.value;

    if(!isStashed) {
      fields = extractOrderAndFilterFields(schema);
      // Cache the ordered/filtered fields to avoid having to do it again.
      ctx[ORDERED_FIELDS_KEY] = fields;
    }

    return mapObjectToFields(fields, values);

  } else if (Array.isArray(values)) {
    return mapColsToFields(fields, values)
  }
}

*/


module.exports = function extractSortAndFilter(schema, filter) {
  // concat the schema's key fields and own fields
  let fields = schema.ownFields.concat(schema.keyFields)
  // remove the fields that doesn't exists on "values" object
  .filter((fld) => {
    return filter[fld.property] && !filter[fld.property].invalid;
  })
  // order the filtered fields by its "field" property
  .sort((fldA, fldB) => {
    if(fldA.field > fldB.field) {
      return 1;

    } else if (fldA.field < fldB.field) {
      return -1;

    } else {
      return 0;
    }
  });

  return fields;
}
