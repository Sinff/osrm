'use strict';

module.exports = function mapValuesToFields (values, fields) {
  let curr;
  let orderedArgs = [];
  let filteredFields = [];
  // A map of fields already put on filteredFields array
  let inserted = {};
  checkValuesAndFieldsLength(values.length, fields.length);

  // Only the last dummy field should stay in the filteredFields list
  for(let i = fields.length -1; i >= 0; i-- ) {
    curr = fields[i];
    values[i].props = curr.customProps;

    // ignores duplicated dummy fields
    if (!curr.isDummy || !inserted[curr.field]) {
      filteredFields.push(curr);
      inserted[curr.field] = true;
    }
  }
  // reverse to restore the original fields order.
  filteredFields.reverse();
  orderedArgs = values;
  return {values: orderedArgs, fields: filteredFields};
}

function checkValuesAndFieldsLength (values, fields) {
  if(values !== fields) {
    throw new Error('The columns and values doesn\'t have the same size. {values: '+values+', fields: '+fields+'}');
  }
}
