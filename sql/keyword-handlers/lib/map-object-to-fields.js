'use strict';

module.exports = function mapObjectToFields(object, fields) {
  let val;
  let orderedArgs = [];
  let filteredFields = [];
  // A map of fields already put on filteredFields array
  let inserted = {};
  let fieldValueMap = {};
  let fvmArr;

  // extract the properties from "object" argument in the same order as the fields array
  fields.forEach((curr) => {
    val = object[curr.property];

    // TODO: THIS SCRIPT IS NOT SUPPOSED TO CREATE WRAPPERS WITH TYPE INFO, IT SHOULD BE REMOVED FROM HERE
    if(!val) {
      val = {invalid: true}
    }
    // copy the custom props from the current field
    val.props = curr.customProps;
    orderedArgs.push(val);

    fvmArr = fieldValueMap[curr.field] = fieldValueMap[curr.field] || []
    fvmArr.length === 0 && fvmArr.push(curr);
    fvmArr.push(val);

    // ignore duplicated dummy fields
    if (!curr.isDummy || !inserted[curr.field]) {
      filteredFields.push(curr);
      inserted[curr.field] = true;
    }
  });
  return {values: orderedArgs, fields: filteredFields, map: fieldValueMap};
}
