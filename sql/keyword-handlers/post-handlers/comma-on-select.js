'use strict';

const CommaHandler = {};

const CONSTS = require('../../constants');
const SECTION_KEY = CONSTS.SECTION_KEY;
const LAST_REQUEST = CONSTS.LAST_REQUEST;


CommaHandler.name = 'comma-on-select';
CommaHandler.isPostHandler = true;

CommaHandler.handler = function (steps, stash) {
  if(steps[0].skip) {
    return steps;
  }

  const kw = steps[0].kw;
  const lastRequest = stash[LAST_REQUEST];
  const currSection = stash[SECTION_KEY];
  const selectSection = kw.keywords.SELECT.section;
  const selectDSection = kw.keywords.SELECT_DISTINCT.section;
  const hasAnyColDeclared = lastRequest && (
    (lastRequest.kw.identifier !== 'SELECT' ||
     lastRequest.kw.identifier !== 'SELECT_DISTINCT') || lastRequest.argsCount > 0);

  if((currSection === selectSection || currSection === selectDSection) && hasAnyColDeclared) {
    steps[0].kw = kw.clone();
    steps[0].kw.keySeparator = ',';
  }
  return steps;
}

module.exports = CommaHandler;
