'use strict';

const JoinHandler = {};

JoinHandler.name = 'join';

JoinHandler.handler = function (kw, args, stash) {
  return { args: args[0], refs: args[1], kw: kw, schema: true, reference: true };
}

module.exports = JoinHandler;
