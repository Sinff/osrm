'use strict';

const util = require('../../lib/util');
const TAG = 'values-update-handler.js';
const assert = util.assert;
const extractSortAndFilter = require('./lib/extract-sort-and-filter');
const mapObjectToFields = require('./lib/map-object-to-fields');

const CONSTS = require('../constants');
const UPDATE_SCHEMA = CONSTS.UPDATE_SCHEMA;

const ValuesHandler = {};

ValuesHandler.name = 'valuesUpdate';

ValuesHandler.handler = function (kw, args, stash) {
  let steps = [];

  const schema = stash[UPDATE_SCHEMA];
  const values = args[0];
  let fields;

  assert(values.object, 'Parameter type mismatch. Expected argument to be a an object but found ' + (values.type || (values.isField && 'field') || (values.isSchema && 'schema')));

  const object = values.value;
  fields = extractSortAndFilter(schema, object);
  const map = mapObjectToFields(object, fields).map;

  const ASSIGN = kw.keywords.ASSIGN;
  Object.keys(map).forEach((key) => {
    steps.push({kw: ASSIGN, args: map[key], resolve: true});
  });

  return steps;
}


module.exports = ValuesHandler;
