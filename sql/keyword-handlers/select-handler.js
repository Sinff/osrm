'use strict';

const SelectHandler = {};

SelectHandler.name = 'select';

SelectHandler.handler = function (kw, args, stash) {
  return {args: args, kw: kw, schema: true};
}

module.exports = SelectHandler;
