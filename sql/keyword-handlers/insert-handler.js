'use strict';

const util = require('../../lib/util');
const assert = util.assert;
const CONSTS = require('../constants');
const INSERT_SCHEMA = CONSTS.INSERT_SCHEMA;

const InsertHandler = {};

InsertHandler.name = 'insert';

// things like inserts and updates goes here
InsertHandler.handler = function(kw, args, stash) {
  assert(args[0].isSchema, 'Parameter type mismatch. The keyword "' + kw.toString() + '" expects a Schema as parameter, but found: ' + (typeof args) );
  stash[INSERT_SCHEMA] = args[0];
  return {args: args[0], kw: kw};
}

module.exports = InsertHandler;
