'use strict';

const util = require('../../lib/util');
const Log = require('../../lib/simple-logger');
const TAG = 'values-insert-handler.js';
const assert = util.assert;
const extractSortAndFilter = require('./lib/extract-sort-and-filter');
const mapValuesToFields = require('./lib/map-values-to-fields');
const mapObjectToFields = require('./lib/map-object-to-fields');


const CONSTS = require('../constants');
const SHOULD_FILTER = CONSTS.SHOULD_FILTER;
const HAS_VALUES_GENERATED = CONSTS.HAS_VALUES_GENERATED;
const FILTER_SCHEMA = CONSTS.FILTER_SCHEMA;
const ORDERED_COLS = CONSTS.ORDERED_COLS;
const INSERT_SCHEMA = CONSTS.INSERT_SCHEMA;

const ValuesHandler = {};

ValuesHandler.name = 'valuesInsert';

ValuesHandler.handler = function (kw, args, stash) {
  let steps, mapped;
  const values = args[0].object ? args[0] : args;
  const schema = stash[INSERT_SCHEMA];
  let fields = stash[ORDERED_COLS];

  if(values.object && (schema || fields)) {
    const object = values.value;
    fields = fields || extractSortAndFilter(schema, object);
    // store the ordered cols to avoid resolving it in multi-values scenario
    stash[ORDERED_COLS] = fields;
    mapped = mapObjectToFields(object, fields);

  } else if (!values.object) {
    !fields && Log.e(TAG, 'There is no way to guess the values order when no COLS was specified.');
    assert(fields, 'You must call COLS keyword when the VALUES parameters are primitives.');
    mapped = mapValuesToFields(values, fields);
  }

  if(stash[HAS_VALUES_GENERATED]) {
    const VALS = kw.keywords.VALS.clone();
    VALS.keySeparator = ',';    

    steps = {stash: stash, args: mapped.values, kw: VALS };

  } else {
    const COLS = kw.keywords.COLS;
    steps = [
      {stash: stash, args: mapped.fields, kw: COLS },
      {stash: stash, args: mapped.values, kw: kw   }
    ];
  }
  stash[HAS_VALUES_GENERATED] = true;

  return steps;
}


module.exports = ValuesHandler;
