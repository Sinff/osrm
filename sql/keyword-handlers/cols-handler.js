'use strict';

const util = require('../../lib/util');
const assert = util.assert;

const CONSTS = require('../constants');
const ORDERED_COLS = CONSTS.ORDERED_COLS;

const ColsHandler = {};

ColsHandler.name = 'cols';

ColsHandler.handler = function(kw, args, stash) {
  validateArgsOrThrow(args, kw);

  stash[ORDERED_COLS] = args;
  // skips the query and schema generation
  return {skip: true};
}

function validateArgsOrThrow(args, kw) {
  const parentAlias = args[0].parent.alias;
  for(let i = 0; i < args.length; i++) {
    assert(args[i].field, 'Parameter type mismatch. The keyword '+ kw.toString() +' expect its arguments to be fields but found ' + (typeof args[i]))
    assert(args[i].parent.alias === parentAlias, 'Multiple schemas not allowed. The keyword '+ kw.toString() +' expect all its arguments to have the same parent schema, but more than one was specified: [' + parentAlias + ', ' + args[i].parent.alias + '] ') ;
  }
}

module.exports = ColsHandler;
