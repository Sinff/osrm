'use strict';

const util = require('../../lib/util');
const assert = util.assert;
const CONSTS = require('../constants');
const UPDATE_SCHEMA = CONSTS.UPDATE_SCHEMA;

const UpdateHandler = {};

UpdateHandler.name = 'update';

// things like inserts and updates goes here
UpdateHandler.handler = function(kw, args, stash) {
  assert(args[0].isSchema, 'Parameter type mismatch. The keyword "' + kw.toString() + '" expects a Schema as parameter, but found: ' + (typeof args) );
  stash[UPDATE_SCHEMA] = args[0];
  return {args: args[0], kw: kw};
}

module.exports = UpdateHandler;
