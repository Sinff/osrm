'use strict';

const CONSTS = require('../constants');
const ASSIGN_CALLED = CONSTS.ASSIGN_CALLED;

const AssignHandler = {};

AssignHandler.name = 'assign';

AssignHandler.handler = function(kw, args, stash) {
  const step = {};

  if(stash[ASSIGN_CALLED]) {
    kw = kw.clone();
    kw.keySeparator = ',';
  }

  stash[ASSIGN_CALLED] = true;
  return {kw: kw, args: args};

}

module.exports = AssignHandler;
