'use strict';

const util = require('../../lib/util');
const assert = util.assert;
const AsHandler = {};
const CONSTS = require('../constants');
const AS_KEY = CONSTS.AS_AND_ALIAS_KEY;
const LAST_REQUEST = CONSTS.LAST_REQUEST;

AsHandler.name = 'as';

AsHandler.handler = function (kw, args, stash) {
  const data = args[0];
  let response;
  stash[AS_KEY] = stash[AS_KEY] || {};
  const req = stash[LAST_REQUEST];

  assert(data && (data.field || data.schema), 'Parameter type mismatch. Expected val to be a Field or Schema, but found ' + (typeof val));

  if(data.field) {
    response = { kw: kw, schema: true, args: data };
    if(req && req.kw.fn) {
      stash[AS_KEY][data.alias] = true;
    }

  } else if (data.schema) {
    response = { kw: kw, args: data };
  }

  return response;
}

module.exports = AsHandler;
