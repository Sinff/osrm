'use strict';

const util = require('../lib/util');
const assert = util.assert;
const Log = require('../lib/simple-logger');
const Core = require('./core/core');
const SqlHandler = require('./core/ctx-sql-handler');
const SchemaHandler = require('./core/ctx-sch-handler');
const Schema = require('./schema/schema');
const DEFAULT_KEYWORDS = require('./keywords');
const DEFAULT_OBJ_PARSERS = [];
const DEFAULT_SQL_PARSERS =  [
  new (require('./sql-parsers/alias-parser')),
  new (require('./sql-parsers/field-parser')),
  new (require('./sql-parsers/schema-parser')),
  new (require('./sql-parsers/string-parser')),
  new (require('./sql-parsers/primitive-parser')),
  new (require('./sql-parsers/array-parser')),
  new (require('./sql-parsers/context-parser'))
]
const DEFAULT_KW_HANDLERS = [
  require('./keyword-handlers/as-handler'),
  require('./keyword-handlers/cols-handler'),
  require('./keyword-handlers/insert-handler'),
  require('./keyword-handlers/join-handler'),
  require('./keyword-handlers/select-handler'),
  require('./keyword-handlers/values-insert-handler'),
  require('./keyword-handlers/values-update-handler'),
  require('./keyword-handlers/assign-handler'),
  require('./keyword-handlers/update-handler'),
  require('./keyword-handlers/post-handlers/comma-on-select')
];

const KEY = 'sql-plugin';
const NAME = 'SQL';
const KEYWORD_PROP_NAME = 'identifier';
// TODO: the extractElements was set to true, but it seems most useful to be false as default. Must check if it should even exists.
const DEFAULT_KEYWORD_PROPS = {
  head: '',
  tail: '',
  keySeparator: ' ',
  bodySeparator: ',',
  headTailSeparator: ' ',
  keyAliasSeparator: 'AS',
  keyPropAccessor: '.',
  label: false,
  name: true,
  prefix: true,
  alias: false,
  extractElements: false,
  depth: 2
}

/**
* SQLPlugin configuration point.
* @param {object} [config] - The configuration object holding the following properties:
* @property {string} [name] - The name that the plugin will be accessed by. (defaults to SQL)
* @property {function} [onExpose] - A callback that receives a reference to the object merged with the plugin.
* Any so any property set to it will be accessible by the plugin name (e.g. SQL.newProp).
* @property {object} [keywordProps] - A property group applyied to all registered keyword.
* @property {array} [sqlParsers] - An array of sql parser objects. {@link /sql/core/sql-parsers/READ.ME}
* @property {object} [objParsers] - A list of named object parsers. {@link /sql/core/obj-parsers/READ.ME}
* @property {object} [keywords] - A list of named custom keywords.
* @property {object} [kwHandlers] - A list of named keyword handlers. {@link /sql/core/keyword-handlers/READ.ME}
* @property {string} [stash] - The context stash key. ( Not very useful yet )
*/
function SQLPlugin (config) {
  assert(this instanceof SQLPlugin, 'SQLPlugin called without new operator');
  config = config || {};
  const keyProps = Object.assign({}, DEFAULT_KEYWORD_PROPS, config.keywordProps);
  const keywords = util.propertiesAsArray (
    Object.assign({}, DEFAULT_KEYWORDS, config.keywords),
    KEYWORD_PROP_NAME
  );

  const sqlParserArr = (config.sqlParsers || []).concat(DEFAULT_SQL_PARSERS);
  const kwHandlerArr = (config.kwHandler || []).concat(DEFAULT_KW_HANDLERS);
  config.objParsers = DEFAULT_OBJ_PARSERS.concat(config.objParsers || []);

  Schema._configure(config);
  const schHandler = new SchemaHandler(Schema);
  const sqlHandler = new SqlHandler();

  const key =  config.stash || KEY;
  const name = config.name  || NAME;

  const core = new Core(key);

  core.setSqlParsers(sqlParserArr);
  core.setKeywordHandlers(kwHandlerArr);
  core.setSqlContextHandler(sqlHandler);
  core.setSchemaContextHandler(schHandler);

  const onCall = core.onCall.bind(core);
  const onMerge = core.onMerge.bind(core);

  this._schHandler = schHandler;
  this._sqlHandler = sqlHandler;
  this._key = key;

  this.config = { keywordProps: keyProps };
  this.onExpose = config.expose;
  this.name = name;
  this.onCall = onCall;
  this.onMerge = onMerge;
  this.keywords = keywords;
  this.expose = this.expose.bind(this);
}

SQLPlugin.prototype.expose = function (obj) {
  const self = this;

  obj.schema = function (source, alias, fields) {
    const schema = new Schema(source, alias, fields);

    // TODO: passing fields as first parameter with ALIAS property set is wrongly creating a new field.
    // Exposes the Entity constructor by its alias property.
    obj[schema.alias] = function (alias, fields) {
      if(util.isObject(alias)) {
        fields = alias;
        alias = fields.ALIAS || schema.alias;
      }

      fields = Object.assign({}, schema.fields, fields);
      let source = schema.name;
      alias = alias || schema.alias;

      // Returns only the fields
      const cloned = new Schema(source, alias, fields);
      return self._schHandler.exposeFields(cloned);
    };

    // Returns only the fields
    return schema.fields;
  }

  obj.toSql = function (context) {
    const stash = context.retrieve(self._key);
    const sql = self._sqlHandler.getSql(stash);

    return sql;
  }

  obj.toObject = function (context, rows)  {
    const stash = context.retrieve(self._key);
    return self._schHandler.resolve(stash, rows);
  }

  this.onExpose && this.onExpose(obj);
}


module.exports = SQLPlugin;
